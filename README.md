# SKAO Project Tracking Tool

The repository for the SKAO’s Project Tracking Tool (PTT) to query ODA database.

## Project Description

The SKAO OSO [Project Tracking Tool](https://gitlab.com/ska-telescope/oso/ska-oso-ptt) is the User Interface for the Project Tracking Tool which is currently under development.

Current [Project Tracking Tool](https://gitlab.com/ska-telescope/oso/ska-oso-ptt) allows users to query SKAO's OSO Data Archive for information on Scheduling Block Definition(SBD),Scheduling Block Instance(SBI) and Execution Block(EB) stored in the database
and links between them (i.e. a certain SBI was generated from a certain SBD, and EB with EBID XX was derived from executing SBI YY) and display the results on the GUI.

## Local development and testing

### Config SKAO repositories to be a part of the React packages search path

To allow for the SKAO libraries to be picked up when you re-install packages,

run `yarn config set @ska-telescope:registry https://artefact.skao.int/repository/npm-internal/`

### Update to the latest SKAO repositories in the project

Run `yarn skao:update` to pull in latest SKAO repository dependencies to the project

### Installing project dependencies

Run `yarn install` to install the latest project dependencies from package.json and yarn.lock

### Running a front-end development server

Run `yarn start` for a dev server and navigate to `http://localhost:8090/`. The
app will automatically reload if you change any of the source files.

To use a PTT backend deployed on localhost please include the back-end URL in the `yarn start` command, for example `BACKEND_URL=http://localhost/ptt/api/v0`.

### Running tests

Run `yarn test:headless` to execute the Cypress tests.

### Running static code analysis

Run `yarn lint` to lint the code.

## Deploying to Kubernetes

The full production system will consist of the PTT accessing PTT REST API which connects with a PostgreSQL instance.

To deploy all of these services, run:

```
make oci-build
```

The umbrella Helm chart can then be deployed with

```
make k8s-install-chart
```

and uninstalled with

```
make k8s-uninstall-chart
```

The `yarn start` script runs two commands: `make set-dev-env-vars` to set the URL for the PTT back-end and `webpack serve` to start the development server.

If using minikube, run `minikube ip` to find the host IP. `KUBE_NAMESPACE` is set to `ska-oso-ptt` by default.  
The backend component will also be deployed to a separate pod, which the web application will make requests to.

The UI should then be available externally at `http://<MINIKUBE_IP>/<KUBE_NAMESPACE>/ptt/` and the back-end URL will be available at `http://<minikube-ip>/ska-oso-ptt/ptt/api/v0`

### Including an unpublished Helm chart for an PTT dependency

The ska-oso-ptt Helm chart depends on the Helm charts of several other components:

1. ska-db-ptt-services-umbrella for PTT REST API.

To use a WIP chart for one of these dependencies, first create a Gitlab token with read_api privileges following
the instructions at https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html. Then, add the Gitlab
Helm repository for the project you want to source an unpublished chart from. For example, to add the Helm
chart repository for the PTT services project, run

```
helm repo add --username <username> --password <gitlab token> ska-oso-ptt-services https://gitlab.com/api/v5/projects/19329547/packages/helm/dev
helm repo update
```

Finally, edit the umbrella chart definition (found in `charts/ska-oso-ptt-umbrella/Chart.yaml`), modifying
the version and repository definitions to point to the dependency chart you want to source.

## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-oso-ptt/badge/?version=latest)](https://developer.skao.int/projects/ska-oso-ptt/en/latest/?badge=latest)

Documentation can be found in the `docs` folder. To build docs, install the
documentation specific requirements:

```
pip3 install -r docs/requirements.txt
```

and build the documentation (will be built in docs/build folder) with

```
make docs-build html
```
