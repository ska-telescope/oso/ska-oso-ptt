const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const deps = require('./package.json').dependencies;

module.exports = () => ({
  entry: './src/index.tsx',
  output: {},

  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },

  resolve: {
    extensions: ['.tsx', '.ts', '.jsx', '.js', '.json'],
  },

  devServer: {
    port: 8090,
    historyApiFallback: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization',
    },
  },

  module: {
    rules: [
      {
        test: /\.m?js|\.jsx/,
        type: 'javascript/auto',
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
      {
        test: /\.(ts|tsx|js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
  devtool: 'source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env.VERSION': JSON.stringify(process.env.npm_package_version),
    }),
    new ModuleFederationPlugin({
      name: 'ptt',
      filename: 'remoteEntry.js',
      remotes: {
        app2: 'app2@http://localhost:8090/remoteEntry.js',
      },
      exposes: {
        './ptt': './src/components/Routing/Routing.tsx',
      },
      shared: {
        ...deps,
        react: {
          eager: true,
          singleton: true,
          requiredVersion: deps.react,
        },
        dotenv: {
          eager: true,
          singleton: true,
          requiredVersion: deps.dotenv,
        },
        formik: {
          eager: true,
          // singleton: true,
          requiredVersion: deps['formik'],
        },
        'json-loader': {
          eager: true,
          singleton: true,
          requiredVersion: deps['json-loader'],
        },
        'react-dom': {
          eager: true,
          singleton: true,
          requiredVersion: deps['react-dom'],
        },
        'react-json-view': {
          eager: true,
          singleton: true,
          requiredVersion: deps['react-json-view'],
        },
        'react-router-dom': {
          eager: true,
          singleton: true,
          requiredVersion: deps['react-router-dom'],
        },
        moment: {
          eager: true,
          singleton: true,
          requiredVersion: deps.moment,
        },
        'moment-timezone': {
          eager: true,
          singleton: true,
          requiredVersion: deps['moment-timezone'],
        },
        // i18n
        i18next: {
          eager: true,
          // singleton: true,
          requiredVersion: deps.i18next,
        },
        'react-i18next': {
          eager: true,
          // singleton: true,
          requiredVersion: deps['react-i18next'],
        },
        'i18next-browser-languagedetector': {
          eager: true,
          singleton: true,
          requiredVersion: deps['i18next-browser-languagedetector'],
        },
        'i18next-http-backend': {
          eager: true,
          singleton: true,
          requiredVersion: deps['i18next-http-backend'],
        },
        '@hookform/resolvers': {
          eager: true,
          requiredVersion: deps['@hookform/resolvers'],
        },
        lodash: {
          eager: true,
          singleton: true,
          requiredVersion: deps.lodash,
        },
        'js-cookie': {
          eager: true,
          singleton: true,
          requiredVersion: deps['js-cookie'],
        },
        zod: {
          // singleton: true,
          eager: true,
          requiredVersion: deps.zod,
        },
        'react-hook-form': {
          eager: true,
          requiredVersion: deps['rreact-hook-form'],
        },
        '@mui/icons-material': {
          singleton: true,
          eager: true,
          requiredVersion: deps['@mui/icons-material'],
        },
        // Material UI
        '@mui/material': {
          singleton: true,
          eager: true,
          requiredVersion: deps['@mui/material'],
        },
        '@tanstack/react-query': {
          singleton: true,
          eager: true,
          requiredVersion: deps['@tanstack/react-query'],
        },
        '@hookform/resolvers/zod': {
          eager: true,
          // singleton: true,
          requiredVersion: deps['@hookform/resolvers/zod'],
        },
        // SKAO components
        '@ska-telescope/ska-gui-components': {
          eager: true,
          requiredVersion: deps['@ska-telescope/ska-gui-components'],
        },
        '@ska-telescope/ska-gui-local-storage': {
          eager: true,
          requiredVersion: deps['@ska-telescope/ska-gui-local-storage'],
        },
        // mixture
        axios: { singleton: true, requiredVersion: '^1.5.1', eager: true },
        '@emotion/react': {
          singleton: true,
          eager: true,
          requiredVersion: deps['@emotion/react'],
        },
        '@emotion/styled': {
          singleton: true,
          eager: true,
          requiredVersion: deps['@emotion/styled'],
        },
      },
    }),
    new HtmlWebPackPlugin({
      template: './public/index.html',
      excludeChunks: ['child', 'mfe-chunk', 'odt'],
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'public',
          globOptions: {
            dot: true,
            gitignore: true,
            ignore: ['**/*.html'],
          },
        },
      ],
    }),
  ],
});
