
Scheduling Block Definition Page
---------------------------------


User can search for Scheduling Block Definition (SBD) by clicking on  |sbdbtn| button and then search using filters like (Quick Filters, Search by Dates or Search by ID).

.. |sbdbtn| image:: /images/sbdButton.png
   :width: 30%
   :alt: Search Button

.. figure:: /images/sbdTableList.png
   :width: 100%
   :align: center
   :alt: Image of the SBD Table

|

Once the entity records are visible in tabular format user can view the Execution Block (EB) associated with SBD ID by clicking on EB ID from column named 'Execution Block ID' in the
table.

This action will navigate user to Execution Block (EB) page as shown in below image.

.. figure:: /images/ebTable.png
   :width: 100%
   :align: center
   :alt: Image of the EB Table

|

User can also view the Scheduling Block Instance (SBI) associated with SBD ID by clicking on SBI ID from column named 'Scheduling Block Instance ID' in the table.

This action will navigate user to Scheduling Block Instance (SBI) page as shown in below image.

.. figure:: /images/sbiTable.png
   :width: 100%
   :align: center
   :alt: Image of the SBI Table

|



View SBD details
~~~~~~~~~~~~~~~~

Once the entity records are visible in tabular format as shown below image user can view the entity details by clicking on view icon displayed in column named 'View' in the table. 


.. figure:: /images/sbdDetails.png
   :width: 100%
   :align: center
   :alt: Image of SBD details

|

Update SBD status
~~~~~~~~~~~~~~~~~

Once the entity records are visible in tabular format as shown below image user can update status of the entity by clicking on edit icon displayed in column named 'Status' in the table. 
Then a model will open in which user can select the status value from the dropdown widget and click on the |updateStatusBtn| button this will update the status of the entity.
User will also be able to view the status history for slected entity in tabular format as shown in below image.


.. |updateStatusBtn| image:: /images/updateStatusBtn.png
   :width: 15%
   :alt: Search Button


.. figure:: /images/updateSbdStatus.png
   :width: 100%
   :align: center
   :alt: Image of manage SBD status

|


