Common Questions
~~~~~~~~~~~~~~~~

FAQs for the Minimal Viable Product (MVP)
=========================================

.. admonition:: How will I know if my entity status has been successfully updated?

   You will receive a success notification and the status of your entity will change from 
   "previous status" to "current status".

.. admonition:: Can I use fuzzy/pattern search for entity ID?

      Yes. You can use fuzzy/pattern match to search records with entity ID for example for EBs search you can search with "eb-mvp01"


.. admonition:: Can I update same status value twice for entities?

   Yes. You update same status value twice for any entities

.. admonition:: Who should I contact if I encounter technical difficulties or have questions about PTT?
   
   User can connect on slack channel like #team-nakshatra and #help-oso for any queries.
