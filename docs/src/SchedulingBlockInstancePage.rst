
Scheduling Block Instance Page
-------------------------------

User can search for Scheduling Block Instance (SBI) by clicking on  |sbibtn| button and then search using filters like (Quick Filters, Search by Dates or Search by ID).

.. |sbibtn| image:: /images/sbiButton.png
   :width: 30%
   :alt: Search Button

.. figure:: /images/sbiTableList.png
   :width: 100%
   :align: center
   :alt: Image of the SBD Table

|

Once the entity records are visible in tabular format user can view the Scheduling Block Definition (SBD) associated with SBI ID by clicking on SBD ID from column named 'Scheduling Block Definition ID' in the table as shown in below image.

This action will navigate user to Scheduling Block Definition (SBD) page as shown in below image.

.. figure:: /images/sbdTable.png
   :width: 100%
   :align: center
   :alt: Image of the SBD Table

|

User can also view the Execution Block (EB) associated with SBI ID by clicking on EB ID from column named 'Execution Block ID' in the table as shown in below image.

This action will navigate user to Execution Block (EB) page as shown in below image.

.. figure:: /images/ebTable.png
   :width: 100%
   :align: center
   :alt: Image of the EB Table

|



View SBI details
~~~~~~~~~~~~~~~~

Once the entity records are visible in tabular format as shown below image user can view the entity details by clicking on view icon displayed in column named 'View' in the table. 



.. figure:: /images/sbiDetails.png
   :width: 100%
   :align: center
   :alt: Image of SBI details

|

Update SBI status
~~~~~~~~~~~~~~~~~

Once the entity records are visible in tabular format as shown below image user can update status of the entity by clicking on edit icon displayed in column named 'Status' in the table. 
Then a model will open in which user can select the status value from the dropdown widget and click on the |updateStatusBtn| button this will update the status of the entity.
User will also be able to view the status history for slected entity in tabular format as shown in below image.


.. |updateStatusBtn| image:: /images/updateStatusBtn.png
   :width: 15%
   :alt: Search Button

.. figure:: /images/updateSbiStatus.png
   :width: 100%
   :align: center
   :alt: Image of manage SBI status

|
