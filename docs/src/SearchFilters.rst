Search filters
----------------

Search by Dates
~~~~~~~~~~~~~~~~


User will be able to enter or select Start Date and End Date form date picker on UI and click on |searchByDateBtn| button to view the results in tabular format as shown below image. 
 

.. |searchByDateBtn| image:: /images/searchByDateBtn.png
   :width: 15%
   :alt: Search Button


.. figure:: /images/searcByDate.png
   :width: 70%
   :align: center
   :alt: Search entity by Start Date and End Date

|


Search by ID
~~~~~~~~~~~~~

User will be able to enter Entity ID in input text field on UI and click on |searchByIDBtn| button  to view the results in tabular format. 
Note: User can also use fuzzy/pattern string to search for entity records.

.. |searchByIDBtn| image:: /images/searchByIDBtn.png
   :width: 15%
   :alt: Search Button


.. figure:: /images/searchByID.png
   :width: 65%
   :align: center
   :alt: Search entity by Entity ID

|


Quick Filters
~~~~~~~~~~~~~~

User will be able to select options form toggle buttons |quickFilter| to view the results in tabular format. 

.. |quickFilter| image:: /images/quickFilter.png
   :width: 20%
   :alt: Search Button