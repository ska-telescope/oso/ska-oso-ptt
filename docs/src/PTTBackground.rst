Introduction
~~~~~~~~~~~~


Project Tracking Tool (PTT) allows users like telescope operators to query ODA using PTT backend to view the details for Projects, SBIs, SBDs and EBs.

It communicates with ska-oso-ptt-services via a REST API
see the `REST API <https://developer.skao.int/projects/ska-oso-ptt-services/en/latest/restapi.html>`_


User will be able to search records for respective entities by selecting radio button options on UI  as shown below

|navigation|


.. |navigation| image:: /images/navigation.png
   :width: 100%
   :alt: Navigation
