
Project Page
---------------------

User can search for Projects by clicking on  |projectBtn| button and then search using filters like (Quick Filters, Search by Dates or Search by ID).

.. |projectBtn| image:: /images/projectBtn.png
   :width: 15%
   :alt: Search Button

.. figure:: /images/projectTableList.png
   :width: 100%
   :align: center
   :alt: Image of the SBD Table

|

Once the entity records are visible in tabular format as shown below image user can view the list of SBDs under Observation block by clicking on Block name displayed in column named 'Observation Block' in the table. 
Then a model will open in which user can view the Block ID and list of SBDs in tabular format.
User can then view specific SBD by clicking on SBD ID as show in below image.

.. figure:: /images/obsBlock.png
   :width: 100%
   :align: center
   :alt: Image of the Observation Block

|

This action will navigate user to Scheduling Block Definition (SBD) page as shown in below image.

.. figure:: /images/sbdTable.png
   :width: 100%
   :align: center
   :alt: Image of the SBD Table

|


View Project details
~~~~~~~~~~~~~~~~~~~~~

Once the entity records are visible in tabular format as shown below image user can view the entity details by clicking on view icon displayed in column named 'View' in the table. 


.. figure:: /images/projectDetails.png
   :width: 100%
   :align: center
   :alt: Image of the Project details

|

Update Project status
~~~~~~~~~~~~~~~~~~~~~~

Once the entity records are visible in tabular format as shown below image user can update status of the entity by clicking on edit icon displayed in column named 'Status' in the table. 
Then a model will open in which user can select the status value from the dropdown widget and click on the |updateStatusBtn| button this will update the status of the entity.
User will also be able to view the status history for slected entity in tabular format as shown in below image.


.. |updateStatusBtn| image:: /images/updateStatusBtn.png
   :width: 15%
   :alt: Search Button

.. figure:: /images/updateProject.png
   :width: 100%
   :align: center
   :alt: Image of manage Project status

|