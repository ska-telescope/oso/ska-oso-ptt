SKAO OSO Project Tracking Tool
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Project Tracking Tool (PTT) allows users like telescope operators to query ODA to view the details for Projects, SBIs, SBDs and EBs.

User will be able to fuzzy search by Project ID, SBI ID, SBD ID and EB ID respectively.
User will also be able to search Project, SBI, SBD and EB by a Start Date and End Date.

By default user will have quick filter for displaying records for Last 7 days and for Today.
User can also update the status for respective entities and view the status history.

It communicates with ska-oso-ptt-services via a REST API
see the `REST API <https://developer.skao.int/projects/ska-oso-ptt-services/en/latest/restapi.html>`_


.. figure:: /images/osoWorkFlow.jpg
   :width: 100%
   :align: center
   :alt: Image of OSO Work Flow

|

.. toctree::
    :maxdepth: 2
    :caption: General
    :hidden:

    Overview.rst

.. toctree::
   :maxdepth: 2
   :caption: DEPLOYING AND CONFIGURING
   :hidden:
   
   
   DuringDevelopment
   RunningBuilding
   deployment_to_kubernetes.rst
   



.. toctree::
   :maxdepth: 2
   :caption: User Guide
   :hidden:

   PTTBackground
   HowTo
   UserQuestions
   UserTroubleShooting

   
.. toctree::
   :maxdepth: 2
   :caption: Application Internals/Developer Docs
   :hidden:

   Requirements
   Installation
   Testing

.. toctree::
   :maxdepth: 1
   :caption: Releases
   :hidden:

   CHANGELOG.rst
