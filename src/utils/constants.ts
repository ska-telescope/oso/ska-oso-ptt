import moment from 'moment';

// Common URLs
export const SPACER_HEADER = 70;
export const SPACER_FOOTER = 70;
export const DATA_STORE_BOX_HEIGHT = 70;
export const SPACER = 50;

export const fucreatedBeforeDatellHeight = () =>
  `calc(100vh - ${SPACER_HEADER + SPACER_FOOTER + SPACER}px)`;
export const tableHeight = () =>
  `calc(100vh - ${SPACER_HEADER + SPACER_FOOTER + DATA_STORE_BOX_HEIGHT + SPACER}px)`;

export const ENTITY = {
  ebs: 'ebs',
  sbds: 'sbds',
  sbis: 'sbis',
  prjs: 'prjs'
};

export const ENTITY_ID = {
  ebs: 'eb_id',
  sbds: 'sbd_id',
  sbis: 'sbi_id',
  prjs: 'prj_id'
};

export const SEARCH_TYPE = {
  last7days: 'last 7 days',
  today: 'today',
  dates: 'dates',
  id: 'id'
};

export const makeUrlPath = (data, entity) => {
  let baseURL: string;
  if (data.id && data.id !== '') {
    baseURL = `${entity}?match_type=contains&entity_id=${data.id}`;
  } else {
    baseURL = `${entity}?match_type=equals&created_before=${data.createdBefore}&created_after=${data.createdAfter}`;
  }
  return baseURL;
};

export const today = moment().utc().toISOString().substring(0, 10);
export const nextdate = moment().utc().add(1, 'days').toISOString().substring(0, 10);
export const createdAfterDate = moment().utc().subtract(7, 'days').toISOString().substring(0, 10);
export const createdBeforeDate = moment().utc().toISOString().substring(0, 10);

export const last7days = {
  createdBefore: createdBeforeDate,
  createdAfter: createdAfterDate
};

export const transformStatusInputToOutput = (statusObj) => {
  return (
    statusObj &&
    Object.values(statusObj).map((value) => ({
      label: value,
      value: value
    }))
  );
};

export const toUTCDateFormat = (value: string) => {
  return moment(value).utc().format('DD-MM-YYYY');
};
export const toUTCDateTimeFormat = (value: string) => {
  return moment(value).utc().format('DD-MM-YYYY HH:mm:ss');
};

export const todayDate = moment().utc().toISOString().substring(0, 10);
export const yesterdayDate = moment().utc().subtract(1, 'days').toISOString().substring(0, 10);
export const last7Date = moment().utc().subtract(7, 'days').toISOString().substring(0, 10);

export const getTodayUTCDateRange = (dateString) => {
  const startDate = moment(dateString).startOf('day').utc().format('YYYY-MM-DD HH:mm:ss.SSSSSS');
  const endDate = moment(dateString).endOf('day').utc().format('YYYY-MM-DD HH:mm:ss.SSSSSS');
  return {
    start: startDate,
    end: endDate
  };
};

export const getUTCDateRange = (start, end) => {
  const startDate = moment(start).startOf('day').utc().format('YYYY-MM-DD HH:mm:ss.SSSSSS');
  const endDate = moment(end).endOf('day').utc().format('YYYY-MM-DD HH:mm:ss.SSSSSS');
  return {
    start: startDate,
    end: endDate
  };
};

import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';

export const THEME = [THEME_DARK, THEME_LIGHT];

export const viewPort = (format = 'pc') => {
  const isPC = () => format === 'pc';
  const xAxis = isPC() ? 1500 : 600;
  const yAxis = isPC() ? 1500 : 600;
  cy.viewport(xAxis, yAxis);
};
