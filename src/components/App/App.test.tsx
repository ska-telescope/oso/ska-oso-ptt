/* eslint-disable no-restricted-syntax */
import React from 'react';
import { mount } from 'cypress/react18';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '../../services/theme/theme';
import App from './App';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<App />', () => {
  for (const theTheme of THEME) {
    it(`Theme ${theTheme}: Renders App`, () => {
      mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <App />
        </ThemeProvider>
      );
    });
  }
});
