import { Grid } from '@mui/material';

import {
  Button,
  ButtonColorTypes,
  ButtonSizeTypes,
  ButtonVariantTypes,
  DateEntry,
  SearchEntry
} from '@ska-telescope/ska-gui-components';
import { useTranslation } from 'react-i18next';
import React, { useEffect, useState } from 'react';
import {
  SEARCH_TYPE,
  getTodayUTCDateRange,
  getUTCDateRange,
  last7Date,
  today,
  todayDate,
  yesterdayDate
} from '../../utils/constants';
import SearchIcon from '@mui/icons-material/Search';

interface FilterProps {
  id: string;
}

interface EntryFieldProps {
  filterstate: FilterProps;
  onFilterData: Function;
}

const Filters = ({ filterstate, onFilterData }: EntryFieldProps) => {
  const { t } = useTranslation('translations');
  const [searchType, setSearchType] = useState<string>(SEARCH_TYPE.last7days);
  const [startDate, setStartDate] = useState(last7Date);
  const [endDate, setEndDate] = useState(yesterdayDate);
  const [id, setId] = useState<string>('');
  const [entityIdDisplay, setEntityIdDisplay] = useState<string>('');
  useEffect(() => {
    if (filterstate) {
      setSearchType(SEARCH_TYPE.id);
      setId(filterstate.id);
      setEntityIdDisplay(filterstate.id);
      setStartDate('');
      setEndDate('');
      const filterStates = {
        id: filterstate.id
      };
      onFilterData(filterStates);
    }
  }, [filterstate]);

  const validateStartDates = () => {
    if (Date.parse(startDate) > Date.parse(today)) {
      return t('msg.errFutureDate');
    }
    if (Date.parse(startDate) > Date.parse(endDate)) {
      return t('msg.errInvalidStartDate');
    }
    return '';
  };

  const validateEndDates = () => {
    if (Date.parse(endDate) > Date.parse(today)) {
      return t('msg.errFutureDate');
    }
    if (Date.parse(startDate) > Date.parse(endDate)) {
      return t('msg.errInvalidEndDate');
    }
    return '';
  };

  const searchById = (type) => {
    setSearchType(type);
    setEntityIdDisplay(id);
    setStartDate('');
    setEndDate('');
    disableDateSearch();
    const filterStates = {
      id
    };
    onFilterData(filterStates);
  };

  const searchByLast7Days = (type) => {
    setSearchType(type);
    setStartDate(last7Date);
    setEndDate(yesterdayDate);
    setId('');
    disableIdSearch();
    disableDateSearch();
    const getDateRange = getUTCDateRange(last7Date, yesterdayDate);
    const createdAfter = getDateRange.start;
    const createdBefore = getDateRange.end;
    const filterStates = {
      createdAfter,
      createdBefore
    };
    onFilterData(filterStates);
  };

  const searchByToday = (type) => {
    setSearchType(type);
    setStartDate(todayDate);
    setEndDate(todayDate);
    disableIdSearch();
    disableDateSearch();
    setId('');
    const getDateRange = getTodayUTCDateRange(todayDate);
    const createdAfter = getDateRange.start;
    const createdBefore = getDateRange.end;

    const filterStates = {
      createdAfter,
      createdBefore
    };
    onFilterData(filterStates);
  };

  const searchByDates = (type) => {
    setSearchType(type);
    disableIdSearch();
    setId('');
    const getDateRange = getUTCDateRange(startDate, endDate);
    const createdAfter = getDateRange.start;
    const createdBefore = getDateRange.end;
    const filterStates = {
      createdAfter,
      createdBefore
    };
    onFilterData(filterStates);
  };

  const disableDateSearch = () => {
    if (
      validateStartDates() === '' &&
      validateEndDates() === '' &&
      startDate !== '' &&
      endDate !== ''
    ) {
      return false;
    }
    return true;
  };

  const disableIdSearch = () => {
    if (id !== '') {
      return false;
    }
    return true;
  };

  const message = () => (
    <div>
      {searchType === SEARCH_TYPE.today && (
        <div>
          <span data-testid="msgToday">
            {t('msg.today')}
            <small>{` (${t('dateFormatTwo', { date: new Date(today) })})`}</small>
          </span>
        </div>
      )}
      {searchType === SEARCH_TYPE.last7days && (
        <span data-testid="msgLastWeek">
          {t('msg.last7days')}
          <small>
            {` (Between ${t('dateFormatTwo', {
              date: new Date(last7Date)
            })} and  ${t('dateFormatTwo', { date: new Date(yesterdayDate) })})`}
          </small>
        </span>
      )}
      {searchType === SEARCH_TYPE.dates && startDate && endDate && (
        <span>
          {t('msg.selectedDates')}
          <small>
            {` (Between ${t('dateFormatTwo', {
              date: new Date(startDate)
            })} and  ${t('dateFormatTwo', { date: new Date(endDate) })})`}
          </small>
        </span>
      )}
      {searchType === SEARCH_TYPE.id && (
        <span>
          {t('msg.fuzzySearchById')}
          <small>{` "${entityIdDisplay} "`}</small>
        </span>
      )}
    </div>
  );

  return (
    <>
      <Grid container spacing={2} justifyContent="left">
        <Grid item xs={12} sm={12} md={1.4} style={{ marginTop: '30px' }}>
          <Button
            icon={<SearchIcon />}
            aria-label={t('ariaLabel.searchButton')}
            color={
              searchType === SEARCH_TYPE.last7days
                ? ButtonColorTypes.Secondary
                : ButtonColorTypes.Inherit
            }
            testId="last7days"
            label={t('label.last7days')}
            size={ButtonSizeTypes.Small}
            variant={ButtonVariantTypes.Contained}
            onClick={() => searchByLast7Days(SEARCH_TYPE.last7days)}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={1.2} style={{ marginTop: '30px' }}>
          <Button
            icon={<SearchIcon />}
            aria-label={t('ariaLabel.searchButton')}
            color={
              searchType === SEARCH_TYPE.today
                ? ButtonColorTypes.Secondary
                : ButtonColorTypes.Inherit
            }
            label={t('label.today')}
            size={ButtonSizeTypes.Small}
            variant={ButtonVariantTypes.Contained}
            onClick={() => searchByToday(SEARCH_TYPE.today)}
            testId="today"
          />
        </Grid>
        <Grid item xs={12} sm={12} md={2}>
          <DateEntry
            ariaDescription={t('ariaLabel.dateDescription')}
            ariaTitle={t('ariaLabel.date')}
            helperText={t('msg.requiredStartDate')}
            testId="dateEntryStart"
            errorText={validateStartDates()}
            label={t('label.startDate')}
            value={startDate}
            setValue={setStartDate}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={2}>
          <DateEntry
            ariaDescription={t('ariaLabel.dateDescription')}
            ariaTitle={t('ariaLabel.date')}
            helperText={t('msg.requiredEndDate')}
            testId="dateEntryEnd"
            errorText={validateEndDates()}
            label={t('label.endDate')}
            value={endDate}
            setValue={setEndDate}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={1.3} sx={{ marginTop: '30px' }}>
          <Button
            icon={<SearchIcon />}
            ariaDescription={t('ariaLabel.searchButtonDescription')}
            disabled={disableDateSearch()}
            color={ButtonColorTypes.Secondary}
            variant={ButtonVariantTypes.Contained}
            testId="dateSearch"
            size={ButtonSizeTypes.Small}
            label={t('label.searchByDates')}
            onClick={() => searchByDates(SEARCH_TYPE.dates)}
            toolTip={t('toolTip.button.dateSearch')}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={2.5}>
          <SearchEntry
            ariaDescription={t('ariaLabel.textDescription')}
            ariaTitle={t('ariaLabel.text')}
            label={t('label.idSearch')}
            testId="searchId"
            value={id}
            helperText={t('msg.requiredEntityId')}
            setValue={setId}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={1.2} sx={{ marginTop: '30px' }}>
          <Button
            icon={<SearchIcon />}
            ariaDescription={t('ariaLabel.searchButtonDescription')}
            disabled={disableIdSearch()}
            color={ButtonColorTypes.Secondary}
            variant={ButtonVariantTypes.Contained}
            testId="idSearch"
            size={ButtonSizeTypes.Small}
            label={t('label.searchById')}
            onClick={() => searchById(SEARCH_TYPE.id)}
            toolTip={t('toolTip.button.idSearch')}
          />
        </Grid>
      </Grid>

      <Grid container spacing={2} sx={{ paddingTop: 2 }} justifyContent="left">
        <Grid item xs={12} sm={12} md={5}>
          <div>{message()}</div>
        </Grid>
      </Grid>
    </>
  );
};

export default Filters;
