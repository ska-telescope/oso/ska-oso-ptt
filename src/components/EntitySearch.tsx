import React, { useState } from 'react';
import { Box } from '@mui/material';
import Filters from './Filters/Filters';
import Navigation from './Navigation/Navigation';
import {
  ENTITY,
  createdAfterDate,
  createdBeforeDate,
  getUTCDateRange,
  last7Date,
  makeUrlPath,
  yesterdayDate
} from '../utils/constants';
import apiService from '../services/apis';
import SBITableList from '../pages/Entities/SchedulingBlockInstance/SBITableList/SBITableList';
import ProjectTableList from '../pages/Entities/Project/ProjectTableList/ProjectTableList';
import SBDTableList from '../pages/Entities/SchedulingBlockDefinition/SBDTableList/SBDTableList';
import EBTableList from '../pages/Entities/ExecutionBlock/EBTableList/EBTableList';

interface EntryFieldProps {
  id: string;
  createdBefore: string;
  createdAfter: string;
}

function EntitySearch() {
  const [dataDetails, setDataDetails] = useState([]);
  const [entityType, setEntityType] = useState<string>('');
  const [filter, setFilter] = useState(null);
  const [state, setFilterState] = useState(null);
  const [selectedTab, setSelectedTab] = useState(null);
  const [entityRef, setEntityRef] = useState(false);

  const fetchEntitiesBlock = async (data, entity) => {
    setDataDetails([]);
    setEntityType(entity);
    setSelectedTab(entity);

    const baseURL = makeUrlPath(data, entity);
    const combinedResults = await apiService.getDataWithStatus(baseURL);
    if (combinedResults.status == 200) {
      setDataDetails(combinedResults.data);
      setEntityRef(false);
    }
  };

  const handleFilterData = async (data: EntryFieldProps) => {
    if (!entityRef) {
      fetchEntitiesBlock(data, entityType);
    }
    setFilter(data);
  };

  const handleNavData = (entity) => {
    if (filter) {
      fetchEntitiesBlock(filter, entity);
    } else {
      const getDateRange = getUTCDateRange(last7Date, yesterdayDate);
      const createdAfter = getDateRange.start;
      const createdBefore = getDateRange.end;
      const filterStates = {
        createdAfter,
        createdBefore
      };
      fetchEntitiesBlock(filterStates, entity);
    }
  };

  const onTriggerFunction = (value, entity) => {
    if (value && entity) {
      setEntityRef(true);
      setSelectedTab(entity);
      const filterStates = {
        id: value
      };
      setFilterState(filterStates);
      fetchEntitiesBlock(filterStates, entity);
    } else {
      const filterStates = {
        createdAfter: createdAfterDate,
        createdBefore: createdBeforeDate
      };
      if (filter) {
        fetchEntitiesBlock(filter, entityType);
      } else {
        fetchEntitiesBlock(filterStates, entityType);
      }
    }
  };

  return (
    <>
      <Navigation onNavData={handleNavData} selectedTab={selectedTab} />
      <Box sx={{ marginTop: 3 }} data-testid="content">
        <Filters onFilterData={handleFilterData} filterstate={state} />
        {entityType === ENTITY.prjs && (
          <ProjectTableList updatedList={onTriggerFunction} data={dataDetails} />
        )}
        {entityType === ENTITY.sbds && (
          <SBDTableList updatedList={onTriggerFunction} data={dataDetails} />
        )}
        {entityType === ENTITY.sbis && (
          <SBITableList updatedList={onTriggerFunction} data={dataDetails} />
        )}
        {entityType === ENTITY.ebs && (
          <EBTableList updatedList={onTriggerFunction} data={dataDetails} />
        )}
      </Box>
    </>
  );
}

export default EntitySearch;
