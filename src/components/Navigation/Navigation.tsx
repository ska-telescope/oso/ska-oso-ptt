import React, { useEffect, useState } from 'react';
import { ButtonColorTypes } from '@ska-telescope/ska-gui-components';
import { FormControlLabel, Radio, RadioGroup } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { ENTITY } from '../../utils/constants';

interface EntryFieldProps {
  onNavData;
  selectedTab;
}

const Navigation = ({ onNavData, selectedTab }: EntryFieldProps) => {
  const [entityType, setEntityType] = useState<string>(ENTITY.prjs);
  const { t } = useTranslation('translations');
  const [selectedValue, setSelectedValue] = React.useState(selectedTab ? selectedTab : ENTITY.prjs);
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedValue(event.target.value);
    setEntityType(event.target.value);
    onNavData(event.target.value);
  };

  useEffect(() => {
    if (selectedTab) {
      setSelectedValue(selectedTab);
      setEntityType(selectedTab);
    } else {
      setSelectedValue(ENTITY.prjs);
      onNavData(entityType);
    }
  }, [selectedTab]);

  return (
    <RadioGroup row>
      <FormControlLabel
        style={{ marginRight: '50px' }}
        value={t('label.prjPanel')}
        control={
          <Radio
            data-testid="prjEntity"
            checked={selectedValue === ENTITY.prjs}
            onChange={handleChange}
            value={ENTITY.prjs}
            color={ButtonColorTypes.Secondary}
          />
        }
        label={t('label.prjPanel')}
      />
      <FormControlLabel
        style={{ marginRight: '50px' }}
        value={t('label.sbdPanel')}
        control={
          <Radio
            data-testid="sbdEntity"
            checked={selectedValue === ENTITY.sbds}
            onChange={handleChange}
            value={ENTITY.sbds}
            color={ButtonColorTypes.Secondary}
          />
        }
        label={t('label.sbdPanel')}
      />
      <FormControlLabel
        style={{ marginRight: '50px' }}
        value={t('label.sbiPanel')}
        control={
          <Radio
            data-testid="sbiEntity"
            checked={selectedValue === ENTITY.sbis}
            onChange={handleChange}
            value={ENTITY.sbis}
            color={ButtonColorTypes.Secondary}
          />
        }
        label={t('label.sbiPanel')}
      />
      <FormControlLabel
        style={{ marginRight: '50px' }}
        value={t('label.ebPanel')}
        control={
          <Radio
            data-testid="ebEntity"
            checked={selectedValue === ENTITY.ebs}
            onChange={handleChange}
            value={ENTITY.ebs}
            color={ButtonColorTypes.Secondary}
          />
        }
        label={t('label.ebPanel')}
      />
    </RadioGroup>
  );
};

export default Navigation;
