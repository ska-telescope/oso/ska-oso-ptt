/* eslint-disable no-restricted-syntax */
import React from 'react';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '../../services/theme/theme';
import Navigation from './Navigation';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<Navigation />', () => {
  for (const theTheme of THEME) {
    it(`Theme ${theTheme}: Renders Navigation`, () => {
      cy.mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <Navigation onNavData={undefined} selectedTab={undefined} />
        </ThemeProvider>
      );
      cy.get('body').then((element) => {
        if (element.find('[data-testid="prjEntity"]').length) {
          cy.get('[data-testid="prjEntity"]').should('be.visible');
          cy.get('[data-testid="prjEntity"]').click({ force: true });
        }
      });
    });
  }
});
