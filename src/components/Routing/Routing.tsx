import React from 'react';
import { Routes, Route, BrowserRouter as Router } from 'react-router-dom';
import EntitySearch from '../EntitySearch';

export const Routing = () => {
  const basename = window.env?.BASE_URL;
  return (
    <Router basename={basename}>
      <Routes>
        <Route path="/" element={<EntitySearch />} />
      </Routes>
    </Router>
  );
};

export default Routing;
