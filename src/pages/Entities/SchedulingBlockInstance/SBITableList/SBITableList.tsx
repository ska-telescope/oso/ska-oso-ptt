/* eslint-disable @typescript-eslint/no-explicit-any */
import { Box } from '@mui/material';
import { DataGrid } from '@ska-telescope/ska-gui-components';
import React from 'react';
import { useTranslation } from 'react-i18next';
import ViewSBI from '../ViewSBI/ViewSBI';
import ViewSBD from '../../SchedulingBlockDefinition/ViewSBD/ViewSBD';
import ViewEB from '../../ExecutionBlock/ViewEB/ViewEB';
import SBIDataModel from '../../../../DataModels/Models/SchedulingBlockInstanceModel';
import ManageEntityStatus from '../../ManageEntityStatus/ManageEntityStatus';
import { ENTITY, toUTCDateFormat } from '../../../../utils/constants';

interface EntryFieldProps {
  data: SBIDataModel[];
  updatedList: any;
}

const SBITableList = ({ data, updatedList }: EntryFieldProps) => {
  const { t } = useTranslation('translations');
  let id = 1;
  data &&
    data.map((row) => {
      row.id = id++;
      return row;
    });
  const onTriggerFunction = (value, entity) => {
    updatedList(value, entity);
  };
  const columns = [
    { field: 'sbi_id', headerName: t('label.sbiId'), width: 235 },
    {
      field: 'sbd_ref',
      headerName: t('label.sbdId'),
      width: 235,
      renderCell: (params) => (
        <ViewSBD
          emitEntityID={() => onTriggerFunction(params.row.sbd_ref, ENTITY.sbds)}
          info={null}
          sbdId={params.row.sbd_ref ? params.row.sbd_ref : null}
        />
      )
    },
    {
      field: 'eb_ref',
      headerName: t('label.ebId'),
      width: 235,
      renderCell: (params) => (
        <ViewEB
          emitEntityID={() => onTriggerFunction(params.row.eb_ref, ENTITY.ebs)}
          info={null}
          ebId={params.row.eb_ref ? params.row.eb_ref : null}
        />
      )
    },
    {
      field: 'created_on',
      headerName: t('label.createdOn'),
      width: 120,
      renderCell: (params) => toUTCDateFormat(params.row.metadata.created_on)
    },
    {
      field: 'created_by',
      headerName: t('label.createdBy'),
      width: 120,
      renderCell: (params) => params.row.created_by || params.row.metadata.created_by
    },
    {
      field: 'status',
      headerName: t('label.currentStatus'),
      width: 170,
      renderCell: (params) => (
        <ManageEntityStatus
          currentStatus={params.row.status}
          updatedStatus={onTriggerFunction}
          entityName={ENTITY.sbis}
          entityId={params.row.sbi_id}
          version={params.row.version || params.row.metadata.version}
        />
      )
    },
    {
      field: 'view',
      headerName: t('label.view'),
      width: 85,
      renderCell: (params) => <ViewSBI info={params.row} sbiId={null} emitEntityID={undefined} />
    }
  ];
  return (
    <Box data-testid="availableData" m={1}>
      <DataGrid
        height={600}
        ariaDescription={t('ariaLabel.gridTableDescription')}
        ariaTitle={t('ariaLabel.gridTable')}
        data-testid={data}
        columns={columns}
        rows={data}
        testId="sbiTable"
      />
    </Box>
  );
};

export default SBITableList;
