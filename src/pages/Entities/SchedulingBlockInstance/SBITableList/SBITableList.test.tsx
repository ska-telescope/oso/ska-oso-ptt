/* eslint-disable no-restricted-syntax */
import React from 'react';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '../../../../services/theme/theme';
import sbiDataList from '../../../../DataModels/DataFiles/sbiMock';
import SBITableList from './SBITableList';
import SBIDataModel from '../../../../DataModels/Models/SchedulingBlockInstanceModel';
import { viewPort } from '../../../../utils/constants';
import { StoreProvider } from '@ska-telescope/ska-gui-local-storage';
import { BrowserRouter } from 'react-router-dom';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<SBDTableList />', () => {
  function mount(theTheme) {
    const mockData: SBIDataModel[] = sbiDataList;
    viewPort();
    cy.mount(
      <StoreProvider>
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <BrowserRouter>
            <SBITableList data={mockData} updatedList={undefined} />
          </BrowserRouter>
        </ThemeProvider>
      </StoreProvider>
    );
  }
  for (const theTheme of THEME) {
    it(`Theme ${theTheme}: Renders SBDTableList`, () => {
      mount(theTheme);
    });
  }
});
