/* eslint-disable @typescript-eslint/no-explicit-any */
import PreviewIcon from '@mui/icons-material/Preview';
import ReactJson from 'react-json-view';
import { Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Button,
  ButtonColorTypes,
  ButtonSizeTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';
import SBIDataModel from '../../../../DataModels/Models/SchedulingBlockInstanceModel';

interface EntryFieldProps {
  sbiId: any;
  info: SBIDataModel;
  emitEntityID;
}

const ViewSBI = ({ sbiId, info, emitEntityID }: EntryFieldProps) => {
  const { t } = useTranslation('translations');
  const [open, setOpen] = useState(false);
  const [data, setData] = useState(null);
  const handleClose = () => setOpen(false);

  const getSbiDetails = () => {
    setData(info);
    setOpen(true);
  };

  const loadEntityPage = (value) => {
    emitEntityID(value);
  };
  return (
    <div>
      {sbiId && info === null ? (
        <span
          data-testid="sbiId"
          style={{ cursor: 'pointer', textDecoration: 'underline' }}
          onClick={() => loadEntityPage(sbiId)}
        >
          {sbiId}
        </span>
      ) : (
        <>
          {info && (
            <PreviewIcon
              aria-label={t('ariaLabel.view')}
              data-testid="iconViewSBI"
              style={{ cursor: 'pointer', marginTop: '14px' }}
              onClick={getSbiDetails}
            />
          )}
        </>
      )}
      <Dialog
        aria-label={t('ariaLabel.dialog')}
        data-testid="dialogSbi"
        sx={{
          '& .MuiDialog-container': {
            '& .MuiPaper-root': {
              width: '100%',
              maxWidth: '1100px' // Set your width here
            }
          }
        }}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="sbiDialogTitle">{t('label.sbiInfo')}</DialogTitle>
        <DialogContent dividers>
          <ReactJson enableClipboard={false} src={data} />
        </DialogContent>
        <DialogActions>
          <Button
            color={ButtonColorTypes.Secondary}
            variant={ButtonVariantTypes.Contained}
            size={ButtonSizeTypes.Small}
            testId="sbiClose"
            label={t('label.close')}
            onClick={handleClose}
            toolTip={t('label.close')}
          />
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ViewSBI;
