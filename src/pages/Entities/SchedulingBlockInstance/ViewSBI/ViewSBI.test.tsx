/* eslint-disable no-restricted-syntax */
import React from 'react';
import { mount } from 'cypress/react18';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '../../../../services/theme/theme';
import sbiDataList from '../../../../DataModels/DataFiles/sbiMock';
import ViewSBI from './ViewSBI';
import SBIDataModel from '../../../../DataModels/Models/SchedulingBlockInstanceModel';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<ViewSBI />', () => {
  for (const theTheme of THEME) {
    it(`Theme ${theTheme}: Renders ViewSBI`, () => {
      const mockData: SBIDataModel = sbiDataList[0];
      mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <ViewSBI info={mockData} sbiId={null} emitEntityID={undefined} />
        </ThemeProvider>
      );
      cy.get('body').then((element) => {
        if (element.find('[data-testid="iconViewSBI"]').length) {
          cy.get('[data-testid="iconViewSBI"]').should('be.visible');
          cy.get('[data-testid="iconViewSBI"]').click({ force: true });
        }
      });
    });

    it(`Theme ${theTheme}: Renders ViewSBI with ID`, () => {
      const mockData: SBIDataModel = sbiDataList[0];
      mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <ViewSBI info={null} sbiId={mockData.sbi_id} emitEntityID={undefined} />
        </ThemeProvider>
      );
      cy.get('body').then((element) => {
        if (element.find('[data-testid="sbiId"]').length) {
          cy.get('[data-testid="sbiId"]').should('be.visible');
          cy.get('[data-testid="sbiId"]').click({ force: true });
        }
      });
    });
  }
});
