/* eslint-disable @typescript-eslint/no-explicit-any */
import { Box } from '@mui/material';
import { DataGrid } from '@ska-telescope/ska-gui-components';
import React from 'react';
import { useTranslation } from 'react-i18next';
import SBDDataModel from '../../../../DataModels/Models/SchedulingBlockDefinitionModel';
import ViewSBD from '../ViewSBD/ViewSBD';
import ViewEB from '../../ExecutionBlock/ViewEB/ViewEB';
import ViewSBI from '../../SchedulingBlockInstance/ViewSBI/ViewSBI';
import ManageEntityStatus from '../../ManageEntityStatus/ManageEntityStatus';
import { ENTITY, toUTCDateFormat } from '../../../../utils/constants';

interface EntryFieldProps {
  data: SBDDataModel[];
  updatedList: any;
}

const SBDTableList = ({ data, updatedList }: EntryFieldProps) => {
  const { t } = useTranslation('translations');
  let id = 1;
  data &&
    data.map((row) => {
      row.id = id++;
      return row;
    });
  const onTriggerFunction = (value, entity) => {
    updatedList(value, entity);
  };
  const ebIDCheck = (params) => {
    return params.row?.sdp_configuration?.execution_block?.eb_id ?? null;
  };
  const sbiIDCheck = (params) => {
    return params.row?.sdp_configuration?.processing_blocks?.[0]?.sbi_refs?.[0] ?? null;
  };

  const columns = [
    { field: 'sbd_id', headerName: t('label.sbdId'), width: 220 },
    {
      field: 'eb_id',
      headerName: t('label.ebId'),
      width: 220,
      renderCell: (params) => (
        <ViewEB
          emitEntityID={() => onTriggerFunction(ebIDCheck(params), ENTITY.ebs)}
          ebId={ebIDCheck(params)}
          info={null}
        />
      )
    },
    {
      field: 'sbi_ref',
      headerName: t('label.sbiId'),
      width: 220,
      renderCell: (params) => (
        <ViewSBI
          emitEntityID={() => onTriggerFunction(sbiIDCheck(params), ENTITY.sbis)}
          sbiId={sbiIDCheck(params)}
          info={null}
        />
      )
    },
    {
      field: 'created_on',
      headerName: t('label.createdOn'),
      width: 100,
      renderCell: (params) => toUTCDateFormat(params.row.metadata.created_on)
    },
    {
      field: 'created_by',
      headerName: t('label.createdBy'),
      width: 100,
      renderCell: (params) => params.row.metadata.created_by
    },
    {
      field: 'status',
      headerName: t('label.currentStatus'),
      width: 200,
      renderCell: (params) => (
        <ManageEntityStatus
          currentStatus={params.row.status}
          updatedStatus={onTriggerFunction}
          entityName={ENTITY.sbds}
          entityId={params.row.sbd_id}
          version={params.row.version || params.row.metadata.version}
        />
      )
    },
    {
      field: 'info',
      headerName: t('label.info'),
      width: 150,
      renderCell: (params) => <ViewSBD sbdId={null} info={params.row} emitEntityID={undefined} />
    }
  ];
  return (
    <Box data-testid="availableData" m={1}>
      <DataGrid
        height={600}
        ariaDescription={t('ariaLabel.gridTableDescription')}
        ariaTitle={t('ariaLabel.gridTable')}
        data-testid={data}
        columns={columns}
        rows={data}
        testId="sbdTable"
      />
    </Box>
  );
};

export default SBDTableList;
