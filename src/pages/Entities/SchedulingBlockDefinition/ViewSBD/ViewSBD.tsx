import ReactJson from 'react-json-view';
import PreviewIcon from '@mui/icons-material/Preview';
import { Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Button,
  ButtonColorTypes,
  ButtonSizeTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';

interface EntryFieldProps {
  sbdId: string;
  info: {};
  emitEntityID;
}

const ViewSBD = ({ sbdId, info, emitEntityID }: EntryFieldProps) => {
  const { t } = useTranslation('translations');
  const [open, setOpen] = useState(false);
  const [data, setData] = useState(null);
  const handleClose = () => setOpen(false);

  const getSbdDetails = () => {
    setData(info);
    setOpen(true);
  };

  const loadEntityPage = (value) => {
    emitEntityID(value);
  };
  return (
    <div>
      {sbdId && info === null ? (
        <span
          data-testid="sbdId"
          style={{ cursor: 'pointer', textDecoration: 'underline' }}
          onClick={() => loadEntityPage(sbdId)}
        >
          {sbdId}
        </span>
      ) : (
        <>
          {info && (
            <PreviewIcon
              aria-label={t('ariaLabel.view')}
              data-testid="iconViewSBD"
              style={{ cursor: 'pointer', marginTop: '14px' }}
              onClick={getSbdDetails}
            />
          )}
        </>
      )}
      <Dialog
        aria-label={t('ariaLabel.dialog')}
        data-testid="dialogSbd"
        sx={{
          '& .MuiDialog-container': {
            '& .MuiPaper-root': {
              width: '100%',
              maxWidth: '1100px' // Set your width here
            }
          }
        }}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle data-testid="sbdDialogTitle">{t('label.sbdInfo')}</DialogTitle>
        <DialogContent dividers>
          <ReactJson enableClipboard={false} src={data} />
        </DialogContent>
        <DialogActions>
          <Button
            color={ButtonColorTypes.Secondary}
            variant={ButtonVariantTypes.Contained}
            size={ButtonSizeTypes.Small}
            testId="sbdClose"
            label={t('label.close')}
            onClick={handleClose}
            toolTip={t('label.close')}
          />
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ViewSBD;
