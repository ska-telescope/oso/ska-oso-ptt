/* eslint-disable no-restricted-syntax */
import React from 'react';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { mount } from 'cypress/react18';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '../../../../services/theme/theme';
import ViewSBD from './ViewSBD';
import sbdDataList from '../../../../DataModels/DataFiles/sbdMock';
import SBDDataModel from '../../../../DataModels/Models/SchedulingBlockDefinitionModel';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<ViewSBD />', () => {
  for (const theTheme of THEME) {
    it(`Theme ${theTheme}: Renders ViewSBD`, () => {
      const mockData: SBDDataModel = sbdDataList[0];
      mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <ViewSBD sbdId={null} info={mockData} emitEntityID={undefined} />
        </ThemeProvider>
      );
      cy.get('body').then((element) => {
        if (element.find('[data-testid="iconViewSBD"]').length) {
          cy.get('[data-testid="iconViewSBD"]').should('be.visible');
          cy.get('[data-testid="iconViewSBD"]').click({ force: true });
        }
      });
    });

    it(`Theme ${theTheme}: Renders ViewSBD with ID`, () => {
      const mockData: SBDDataModel = sbdDataList[0];
      mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <ViewSBD sbdId={mockData.sbd_id} info={null} emitEntityID={undefined} />
        </ThemeProvider>
      );
      cy.get('body').then((element) => {
        if (element.find('[data-testid="sbdId"]').length) {
          cy.get('[data-testid="sbdId"]').should('be.visible');
          cy.get('[data-testid="sbdId"]').click({ force: true });
        }
      });
    });
  }
});
