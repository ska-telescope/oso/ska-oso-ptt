/* eslint-disable @typescript-eslint/no-explicit-any */
import { Box } from '@mui/material';
import { DataGrid } from '@ska-telescope/ska-gui-components';
import React from 'react';
import { useTranslation } from 'react-i18next';
import ViewEB from '../ViewEB/ViewEB';
import ViewSBD from '../../SchedulingBlockDefinition/ViewSBD/ViewSBD';
import EBDataModel from '../../../../DataModels/Models/ExecutionBlockModel';
import ViewSBI from '../../SchedulingBlockInstance/ViewSBI/ViewSBI';
import ManageEntityStatus from '../../ManageEntityStatus/ManageEntityStatus';
import { ENTITY, toUTCDateFormat } from '../../../../utils/constants';

interface EntryFieldProps {
  data: EBDataModel[];
  updatedList: any;
}

const EBTableList = ({ data, updatedList }: EntryFieldProps) => {
  const { t } = useTranslation('translations');
  let id = 1;
  data &&
    data.map((row) => {
      row.id = id++;
      return row;
    });

  const onTriggerFunction = (value, entity) => {
    updatedList(value, entity);
  };
  const columns = [
    { field: 'eb_id', headerName: t('label.ebId'), width: 220 },
    {
      field: 'sbd_ref',
      headerName: t('label.sbdId'),
      width: 220,
      renderCell: (params) => (
        <ViewSBD
          emitEntityID={() => onTriggerFunction(params.row.sbd_ref, ENTITY.sbds)}
          sbdId={params.row.sbd_ref ? params.row.sbd_ref : null}
          info={null}
        />
      )
    },
    {
      field: 'sbi_ref',
      headerName: t('label.sbiId'),
      width: 220,
      renderCell: (params) => (
        <ViewSBI
          emitEntityID={() => onTriggerFunction(params.row.sbi_ref, ENTITY.sbis)}
          sbiId={params.row.sbi_ref ? params.row.sbi_ref : null}
          info={null}
        />
      )
    },
    {
      field: 'created_on',
      headerName: t('label.createdOn'),
      width: 120,
      renderCell: (params) => toUTCDateFormat(params.row.metadata.created_on)
    },
    {
      field: 'created_by',
      headerName: t('label.createdBy'),
      width: 120,
      renderCell: (params) => params.row.metadata.created_by
    },
    {
      field: 'status',
      headerName: t('label.currentStatus'),
      width: 200,
      renderCell: (params) => (
        <ManageEntityStatus
          currentStatus={params.row.status}
          updatedStatus={onTriggerFunction}
          entityName={ENTITY.ebs}
          entityId={params.row.eb_id}
          version={params.row.version || params.row.metadata.version}
        />
      )
    },
    {
      field: 'view',
      headerName: t('label.view'),
      width: 150,
      renderCell: (params) => <ViewEB ebId={null} info={params.row} emitEntityID={undefined} />
    }
  ];
  return (
    <Box data-testid="availableData" m={1}>
      <DataGrid
        height={600}
        ariaDescription={t('ariaLabel.gridTableDescription')}
        ariaTitle={t('ariaLabel.gridTable')}
        data-testid={data}
        columns={columns}
        rows={data}
        testId="ebTable"
      />
    </Box>
  );
};

export default EBTableList;
