/* eslint-disable no-restricted-syntax */
import React from 'react';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '../../../../services/theme/theme';
import EBTableList from './EBTableList';
import ebDataList from '../../../../DataModels/DataFiles/ebMock';
import EBDataModel from '../../../../DataModels/Models/ExecutionBlockModel';
import { viewPort } from '../../../../utils/constants';
import { StoreProvider } from '@ska-telescope/ska-gui-local-storage';
import { BrowserRouter } from 'react-router-dom';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<EBTableList />', () => {
  function mount(theTheme) {
    const mockData: EBDataModel[] = ebDataList;
    viewPort();
    cy.mount(
      <StoreProvider>
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <BrowserRouter>
            <EBTableList data={mockData} updatedList={undefined} />
          </BrowserRouter>
        </ThemeProvider>
      </StoreProvider>
    );
  }
  for (const theTheme of THEME) {
    it(`Theme ${theTheme}: Renders EBTableList`, () => {
      mount(theTheme);
    });
  }
});
