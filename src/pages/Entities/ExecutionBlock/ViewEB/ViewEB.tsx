/* eslint-disable @typescript-eslint/no-explicit-any */
import ReactJson from 'react-json-view';
import { Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Button,
  ButtonColorTypes,
  ButtonSizeTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';
import PreviewIcon from '@mui/icons-material/Preview';
import EBDataModel from '../../../../DataModels/Models/ExecutionBlockModel';

interface EntryFieldProps {
  ebId: any;
  info: EBDataModel;
  emitEntityID;
}

const ViewEB = ({ ebId, info, emitEntityID }: EntryFieldProps) => {
  const { t } = useTranslation('translations');
  const [open, setOpen] = useState(false);
  const [data, setData] = useState(null);
  const handleClose = () => setOpen(false);

  const getEbDetails = () => {
    setData(info);
    setOpen(true);
  };
  const loadEntityPage = (value) => {
    emitEntityID(value);
  };
  return (
    <div>
      {ebId && info === null ? (
        <span
          data-testid="ebId"
          style={{ cursor: 'pointer', textDecoration: 'underline' }}
          onClick={() => loadEntityPage(ebId)}
        >
          {ebId}
        </span>
      ) : (
        <>
          {info && (
            <PreviewIcon
              aria-label={t('ariaLabel.view')}
              data-testid="iconViewEB"
              style={{ cursor: 'pointer', marginTop: '14px' }}
              onClick={getEbDetails}
            />
          )}
        </>
      )}
      <Dialog
        aria-label={t('ariaLabel.dialog')}
        data-testid="dialogEb"
        sx={{
          '& .MuiDialog-container': {
            '& .MuiPaper-root': {
              width: '100%',
              maxWidth: '1100px' // Set your width here
            }
          }
        }}
        open={open}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="ebDialogTitle">{t('label.ebInfo')}</DialogTitle>
        <DialogContent dividers>
          <ReactJson enableClipboard={false} src={data} />
        </DialogContent>
        <DialogActions>
          <Button
            color={ButtonColorTypes.Secondary}
            variant={ButtonVariantTypes.Contained}
            size={ButtonSizeTypes.Small}
            testId="ebClose"
            label={t('label.close')}
            onClick={handleClose}
            toolTip={t('label.close')}
          />
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ViewEB;
