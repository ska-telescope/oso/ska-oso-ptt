/* eslint-disable no-restricted-syntax */
import React from 'react';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { mount } from 'cypress/react18';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import ebDataList from '../../../../DataModels/DataFiles/ebMock';
import theme from '../../../../services/theme/theme';
import ViewEB from './ViewEB';
import EBDataModel from '../../../../DataModels/Models/ExecutionBlockModel';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<ViewEB />', () => {
  for (const theTheme of THEME) {
    it(`Theme ${theTheme}: Renders ViewEB`, () => {
      const mockData: EBDataModel = ebDataList[0];
      mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <ViewEB ebId={null} info={mockData} emitEntityID={undefined} />
        </ThemeProvider>
      );
      cy.get('body').then((element) => {
        if (element.find('[data-testid="iconViewEB"]').length) {
          cy.get('[data-testid="iconViewEB"]').should('be.visible');
          cy.get('[data-testid="iconViewEB"]').click({ force: true });
        }
      });
    });
    it(`Theme ${theTheme}: Renders ViewEB with ID`, () => {
      const mockData: EBDataModel = ebDataList[0];
      mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <ViewEB ebId={mockData.eb_id} info={null} emitEntityID={undefined} />
        </ThemeProvider>
      );
      cy.get('body').then((element) => {
        if (element.find('[data-testid="ebId"]').length) {
          cy.get('[data-testid="ebId"]').should('be.visible');
          cy.get('[data-testid="ebId"]').click({ force: true });
        }
      });
    });
  }
});
