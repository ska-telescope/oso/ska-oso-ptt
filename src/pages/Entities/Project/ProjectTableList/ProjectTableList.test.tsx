/* eslint-disable no-restricted-syntax */
import React from 'react';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { mount } from 'cypress/react18';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '../../../../services/theme/theme';
import ProjectTableList from './ProjectTableList';
import ProjectDataModel from '../../../../DataModels/Models/ProjectBlockModel';
import projectDataList from '../../../../DataModels/DataFiles/projectMock';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<ProjectTableList />', () => {
  for (const theTheme of THEME) {
    it(`Theme ${theTheme}: Renders ProjectTableList`, () => {
      const mockData: ProjectDataModel[] = projectDataList;
      mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <ProjectTableList data={mockData} updatedList={undefined} />
        </ThemeProvider>
      );
    });
  }
});
