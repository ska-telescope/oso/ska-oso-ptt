/* eslint-disable @typescript-eslint/no-explicit-any */
import { Box, Typography } from '@mui/material';
import { DataGrid } from '@ska-telescope/ska-gui-components';
import React from 'react';
import { useTranslation } from 'react-i18next';
import ManageEntityStatus from '../../ManageEntityStatus/ManageEntityStatus';
import { ENTITY, toUTCDateFormat } from '../../../../utils/constants';
import ViewProject from '../ViewProject/ViewProject';
import ProjectDataModel from '../../../../DataModels/Models/ProjectBlockModel';
import ViewObsBlock from '../ViewObsBlock/ViewObsBlock';

interface EntryFieldProps {
  data: ProjectDataModel[];
  updatedList: any;
}

const ProjectId = ({ data }) => (
  <Typography sx={{ paddingTop: 1, fontSize: '14px' }}>{data}</Typography>
);
const CreatedDate = ({ data }) => (
  <Typography sx={{ paddingTop: 1, fontSize: '14px' }}>{toUTCDateFormat(data)}</Typography>
);
const User = ({ data }) => <Typography sx={{ paddingTop: 1, fontSize: '14px' }}>{data}</Typography>;

const ProjectTableList = ({ data, updatedList }: EntryFieldProps) => {
  const { t } = useTranslation('translations');
  let id = 1;
  data &&
    data.map((row) => {
      row.id = id++;
      return row;
    });

  const onTriggerFunction = (value, item) => {
    updatedList(value, item);
  };

  const columns = [
    {
      field: 'prj_id',
      headerName: t('label.prjId'),
      width: 235,
      renderCell: (params) => <ProjectId data={params.row.prj_id} />
    },
    {
      field: 'obs_blocks',
      headerName: t('label.obsblocks'),
      width: 235,
      renderCell: (params) => (
        <ViewObsBlock
          obsBlock={params.row.obs_blocks}
          emitEntityID={(value, entity) => onTriggerFunction(value, entity)}
        />
      )
    },
    {
      field: 'created_on',
      headerName: t('label.createdOn'),
      width: 120,
      padding: '20px',
      renderCell: (params) => <CreatedDate data={params.row.metadata.created_on} />
    },
    {
      field: 'created_by',
      headerName: t('label.createdBy'),
      width: 200,
      renderCell: (params) => <User data={params.row.metadata.created_by} />
    },
    {
      field: 'status',
      headerName: t('label.currentStatus'),
      width: 200,
      renderCell: (params) =>
        params.row.status ? (
          <ManageEntityStatus
            currentStatus={params.row.status}
            updatedStatus={onTriggerFunction}
            entityName={ENTITY.prjs}
            entityId={params.row.prj_id}
            version={params.row.metadata.version}
          />
        ) : null
    },
    {
      field: 'view',
      headerName: t('label.view'),
      width: 100,
      renderCell: (params) => <ViewProject info={params.row} />
    }
  ];

  return (
    <Box data-testid="availableData" m={1}>
      <DataGrid
        height={600}
        ariaDescription={t('ariaLabel.gridTableDescription')}
        ariaTitle={t('ariaLabel.gridTable')}
        getRowHeight={() => 'auto'}
        columns={columns}
        rows={data}
        testId="projectTable"
      />
    </Box>
  );
};

export default ProjectTableList;
