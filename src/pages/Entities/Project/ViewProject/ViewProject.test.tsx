/* eslint-disable no-restricted-syntax */
import React from 'react';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { mount } from 'cypress/react18';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import projectDataList from '../../../../DataModels/DataFiles/projectMock';
import theme from '../../../../services/theme/theme';
import ProjectDataModel from '../../../../DataModels/Models/ProjectBlockModel';
import ViewProject from './ViewProject';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<ViewProject />', () => {
  for (const theTheme of THEME) {
    it(`Theme ${theTheme}: Renders ViewProject`, () => {
      const mockData: ProjectDataModel = projectDataList[0];
      mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <ViewProject info={mockData} />
        </ThemeProvider>
      );

      cy.get('body').then((element) => {
        if (element.find('[data-testid="iconViewProject"]').length) {
          cy.get('[data-testid="iconViewProject"]').should('be.visible');
          cy.get('[data-testid="iconViewProject"]').click({ force: true });
          cy.get('[data-testid="projectDialogTitle"]').should('be.visible');
          cy.get('[data-testid="projectDialogTitle"]').contains('label.projectView');
          cy.get('[data-testid="projectClose"]').click();
          cy.get('[data-testid="dialogProject"]').should('not.exist');
        }
      });
    });
  }
});
