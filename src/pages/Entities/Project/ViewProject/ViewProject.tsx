import ReactJson from 'react-json-view';
import { Box, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Button,
  ButtonColorTypes,
  ButtonSizeTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';
import PreviewIcon from '@mui/icons-material/Preview';
import ProjectDataModel from '../../../../DataModels/Models/ProjectBlockModel';

interface EntryFieldProps {
  info: ProjectDataModel;
}

const ViewProject = ({ info }: EntryFieldProps) => {
  const { t } = useTranslation('translations');
  const [open, setOpen] = useState(false);
  const [data, setData] = useState(null);
  const handleClose = () => setOpen(false);
  const getEbDetails = () => {
    setData(info);
    setOpen(true);
  };

  return (
    <div>
      <PreviewIcon
        aria-label={t('ariaLabel.view')}
        data-testid="iconViewProject"
        style={{ cursor: 'pointer', marginTop: '10px' }}
        onClick={getEbDetails}
      />
      <Dialog
        aria-label={t('ariaLabel.dialog')}
        data-testid="dialogProject"
        sx={{
          '& .MuiDialog-container': {
            '& .MuiPaper-root': {
              width: '100%',
              maxWidth: '1100px'
            }
          }
        }}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle data-testid="projectDialogTitle">{t('label.projectView')}</DialogTitle>
        <DialogContent dividers>
          <Box data-testid="availableData" m={1}>
            <ReactJson enableClipboard={false} src={data} />
          </Box>
        </DialogContent>
        <DialogActions>
          <Button
            color={ButtonColorTypes.Secondary}
            variant={ButtonVariantTypes.Contained}
            size={ButtonSizeTypes.Small}
            testId="projectClose"
            label={t('label.close')}
            onClick={handleClose}
            toolTip={t('label.close')}
          />
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ViewProject;
