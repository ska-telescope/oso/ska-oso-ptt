import {
  Box,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography
} from '@mui/material';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Button,
  ButtonColorTypes,
  ButtonSizeTypes,
  ButtonVariantTypes,
  DataGrid
} from '@ska-telescope/ska-gui-components';
import { ENTITY } from '../../../../utils/constants';

interface ObsBlockProps {
  obs_block_id: string;
  name: string;
  sbd_ids: string[];
}

interface EntryFieldProps {
  obsBlock: ObsBlockProps[];
  emitEntityID;
}

const ObsBlockId = ({ data }) => (
  <Typography sx={{ paddingTop: 1, fontSize: '14px' }}>{data}</Typography>
);

const ViewObsBlock = ({ obsBlock, emitEntityID }: EntryFieldProps) => {
  const { t } = useTranslation('translations');
  const [open, setOpen] = useState(false);
  const [data, setData] = useState([]);
  const handleClose = () => setOpen(false);

  const loadEntityPage = (value) => {
    emitEntityID(value, ENTITY.sbds);
    setOpen(false);
  };
  const observationBlockView = (value) => {
    let id = 1;
    value.id = id++;
    setData([value]);
    setOpen(true);
  };
  const SBDIDS = ({ sbddata }) =>
    sbddata.map((item: string) => (
      <Typography
        data-testid="sbdIds"
        key={item}
        style={{ cursor: 'pointer', textDecoration: 'underline', padding: 5 }}
        onClick={() => loadEntityPage(item)}
      >
        {item}
      </Typography>
    ));

  const columns = [
    {
      field: 'obs_block_id',
      headerName: t('label.blockId'),
      width: 235,
      renderCell: (params) => <ObsBlockId data={params.row.obs_block_id} />
    },
    {
      field: 'sbd_id',
      headerName: t('label.sbdId'),
      width: 400,
      renderCell: (param) => <SBDIDS sbddata={param.row.sbd_ids ? param.row.sbd_ids : []} />
    }
  ];
  return (
    <div>
      {obsBlock &&
        obsBlock.map((item: { name: string }, index) => (
          <Typography
            data-testid={`obsBlock${index}`}
            key={obsBlock[index].obs_block_id}
            style={{ cursor: 'pointer', textDecoration: 'underline', padding: 5 }}
            onClick={() => observationBlockView(item)}
          >
            {item.name}
          </Typography>
        ))}

      <Dialog
        aria-label={t('ariaLabel.dialog')}
        data-testid="dialogSbd"
        sx={{
          '& .MuiDialog-container': {
            '& .MuiPaper-root': {
              width: '100%',
              maxWidth: '1100px'
            }
          }
        }}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle>
          <span id="projectDialogTitle">{t('label.obsblocks')}</span>
          <Chip
            sx={{
              position: 'absolute',
              right: 15,
              top: 15
            }}
            color="secondary"
            data-testid="blockName"
            label={`${t('label.blockName')}: ${data && data.length > 0 && data[0].name}`}
          />
        </DialogTitle>
        <DialogContent dividers>
          <Box data-testid="availableData" m={1}>
            <DataGrid
              ariaDescription={t('ariaLabel.gridTableDescription')}
              ariaTitle={t('ariaLabel.gridTable')}
              getRowHeight={() => 'auto'}
              data-testid={data}
              columns={columns}
              rows={data}
              testId="prjSbdTable"
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Button
            color={ButtonColorTypes.Secondary}
            variant={ButtonVariantTypes.Contained}
            size={ButtonSizeTypes.Small}
            testId="sbdObsBlockClose"
            label={t('label.close')}
            onClick={handleClose}
            toolTip={t('label.close')}
          />
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ViewObsBlock;
