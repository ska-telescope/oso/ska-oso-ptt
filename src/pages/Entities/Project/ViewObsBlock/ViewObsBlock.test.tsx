/* eslint-disable no-restricted-syntax */
import React from 'react';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { mount } from 'cypress/react18';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '../../../../services/theme/theme';
import ViewObsBlock from './ViewObsBlock';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<ManageEntityStatuscy />', () => {
  for (const theTheme of THEME) {
    it(`Theme ${theTheme}: Renders ViewObsBlock`, () => {
      const mockEmitEntityID = [
        {
          name: 'Block 1',
          obs_block_id: 'ob-1',
          sbd_ids: ['sbd-t0001-20241007-00003', 'sbd-t0001-20241007-00005']
        }
      ]; // Mock function for emitEntityID
      mount(
        <ThemeProvider theme={theme(THEME_DARK)}>
          <CssBaseline />
          <ViewObsBlock obsBlock={mockEmitEntityID} emitEntityID={undefined} />
        </ThemeProvider>
      );
      cy.get('body').then((element) => {
        if (element.find('[data-testid="obsBlock0"]').length) {
          cy.get('[data-testid="obsBlock0"]').should('be.visible');
          cy.get('[data-testid="obsBlock0"]').click({ force: true });
          cy.get('[data-testid="dialogSbd"]').should('be.visible');
          cy.get('#projectDialogTitle').should('be.visible');
          cy.get('[data-testid="availableData"]').should('be.visible');
          cy.get('[data-testid="sbdObsBlockClose"]').should('be.visible');
          cy.get('[data-testid="sbdObsBlockClose"]').click();
          cy.get('[data-testid="dialogSbd"]').should('not.exist');
        }
      });
    });
  }
});
