/* eslint-disable no-restricted-syntax */
import React from 'react';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '../../../services/theme/theme';
import ManageEntityStatus from './ManageEntityStatus';
import { ENTITY } from '../../../utils/constants';
import statusHistory from '../../../DataModels/DataFiles/statusMock';
import StatusDataModel from '../../../DataModels/Models/StatusBlockModel';
import { BrowserRouter } from 'react-router-dom';
import { StoreProvider } from '@ska-telescope/ska-gui-local-storage';
import { viewPort } from '../../../utils/constants';

const THEME = [THEME_DARK, THEME_LIGHT];

function mount(theTheme) {
  const mockData: StatusDataModel = statusHistory[0];
  viewPort();
  cy.mount(
    <StoreProvider>
      <ThemeProvider theme={theme(theTheme)}>
        <CssBaseline />
        <BrowserRouter>
          <ManageEntityStatus
            currentStatus="Created"
            entityId={mockData.eb_ref}
            entityName={ENTITY.ebs}
            version={mockData.metadata.version}
            updatedStatus={undefined}
          />
        </BrowserRouter>
      </ThemeProvider>
    </StoreProvider>
  );
}

describe('<ManageEntityStatus />', () => {
  beforeEach(() => {
    viewPort();
    mount(THEME[1]);
  });

  for (const theTheme of THEME) {
    it(`Theme ${theTheme}: Renders`, () => {
      mount(theTheme);
    });
  }
  it(`Verify target elements`, () => {
    mount(THEME[1]);
    cy.get('body').then((element) => {
      if (element.find('[data-testid="manageEntityStatus"]').length) {
        cy.get('[data-testid="manageEntityStatus"]').should('be.visible');
        cy.get('[data-testid="manageEntityStatus"]').click();
      }
    });
    cy.get('body').then((element) => {
      if (element.find('[data-testid="dialogStatus"]').length) {
        cy.get('[data-testid="dialogStatus"]').should('be.visible');
        cy.get('[data-testid="statusDialogTitle"]').contains('label.manageStatus');
        cy.get('[data-testid="dialogStatus"]').should('be.visible');
        cy.get('[data-testid="statusDialogTitle"]').should('be.visible');
        cy.get('[data-testid="currentStatus"]').should('be.visible');
        cy.get('[data-testid="statusClose"]').should('be.visible');
        cy.get('[data-testid="currentStats"]').should('contain', 'label.currentStatus: ');
        cy.get('[data-testid="statusUpdate"]').click();
        cy.get('[data-testid="statusClose"]').click();
      }
    });
  });
});
