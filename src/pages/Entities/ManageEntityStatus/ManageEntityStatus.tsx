import DriveFileRenameOutlineIcon from '@mui/icons-material/DriveFileRenameOutline';
import { Dialog, DialogActions, DialogContent, DialogTitle, Grid, Box, Chip } from '@mui/material';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Button,
  ButtonColorTypes,
  ButtonVariantTypes,
  InfoCard,
  InfoCardColorTypes,
  DropDown,
  ButtonSizeTypes
} from '@ska-telescope/ska-gui-components';
import apiService from '../../../services/apis';
import StatusHistoryTableList from './StatusHistoryTableList/StatusHistoryTableList';
import { transformStatusInputToOutput } from '../../../utils/constants';

interface EntryFieldProps {
  currentStatus;
  entityId: string;
  entityName: string;
  version: number;
  updatedStatus;
}

interface StatusRequest {
  current_status: string;
  previous_status: string;
  eb_version?: number;
  sbd_version?: number;
  sbi_version?: number;
  prj_version?: number;
}

const ManageEntityStatus = ({
  currentStatus,
  entityId,
  version,
  entityName,
  updatedStatus
}: EntryFieldProps) => {
  const { t } = useTranslation('translations');
  const [openModal, setOpenModal] = React.useState(false);
  const [data, setData] = useState([]);
  const [showElement, setShowElement] = React.useState(false);
  const [entityStatus, setEntityStatus] = React.useState([]);
  const entityID = entityId;
  const [status, setStatus] = React.useState('');
  const handleClose = () => {
    setOpenModal(false);
    updatedStatus();
  };

  const fetchData = async () => {
    const baseURL = `status/history/${entityName}?entity_id=${entityId}&version=${version}`;
    if (entityId) {
      const response = await apiService.getEntityData(baseURL);
      if (response.status === 200) {
        setData(response.data);
        setStatus(response.data[response.data.length - 1].current_status);
      }
      setOpenModal(true);
    }
  };

  const fetchEntityStatus = async () => {
    const baseURL = `get_entity?entity_name=${entityName.substring(0, entityName.length - 1)}`;
    const response = await apiService.getEntityData(baseURL);
    if (response.status === 200) {
      setEntityStatus(transformStatusInputToOutput(response.data));
    }
  };

  const getVersion = () => {
    const versionMap = {
      ebs: { eb_version: version },
      sbis: { sbi_version: version },
      sbds: { sbd_version: version },
      prjs: { prj_version: version }
    };

    return versionMap[entityName] || {};
  };

  const updateStatus = async () => {
    const statusRequest: StatusRequest = {
      current_status: status,
      previous_status: data[data.length - 1].current_status,
      ...getVersion()
    };

    const baseURL = `status/${entityName}/${entityId}`;
    const response = await apiService.saveStatusEntity(baseURL, statusRequest);
    if (response.status === 200) {
      setShowElement(true);
      setTimeout(() => {
        setShowElement(false);
      }, 3000);
      fetchData();
    }
  };
  const manageStatus = () => {
    if (entityId) {
      fetchEntityStatus();
      fetchData();
    }
  };

  const renderMessageResponce = () => {
    return (
      <InfoCard
        fontSize={20}
        color={InfoCardColorTypes.Success}
        message={t('msg.statusSuccess')}
        testId="successStatusMsg"
      />
    );
  };

  const disableStatusSearch = () => {
    if (data && data.length > 0 && status === data[data.length - 1].current_status) {
      return true;
    }
    return false;
  };

  return (
    <>
      {currentStatus}
      &nbsp;&nbsp;|&nbsp;
      <DriveFileRenameOutlineIcon
        data-testid="manageEntityStatus"
        style={{ cursor: 'pointer', position: 'relative', top: '7px' }}
        onClick={manageStatus}
      />
      <Dialog
        aria-label={t('ariaLabel.dialog')}
        data-testid="dialogStatus"
        sx={{
          '& .MuiDialog-container': {
            '& .MuiPaper-root': {
              width: '100%',
              maxWidth: '1000px' // Set your width here
            }
          }
        }}
        open={openModal}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle>
          <span data-testid="statusDialogTitle">{t('label.manageStatus')}</span>
          &nbsp;
          {entityID}
          <Chip
            sx={{
              position: 'absolute',
              right: 15,
              top: 15
            }}
            color="secondary"
            data-testid="currentStatus"
            label={`${t('label.currentStatus')}: ${
              data && data.length > 0 && data[data.length - 1].current_status
            }`}
          />
        </DialogTitle>
        <DialogContent dividers>
          <Grid
            container
            spacing={2}
            justifyContent="left"
            sx={{ marginTop: '5px', marginBottom: '25px' }}
          >
            <Grid item xs={12} sm={6} md={3}>
              <Box sx={{ minWidth: 170 }}>
                <DropDown
                  options={entityStatus}
                  testId="entityStatus"
                  value={status}
                  setValue={setStatus}
                  label={t('label.currentStatus')}
                  labelBold
                  required
                />
              </Box>
            </Grid>
            <Grid item xs={12} sm={6} md={3} sx={{ marginTop: '20px' }}>
              <Button
                disabled={disableStatusSearch()}
                color={ButtonColorTypes.Secondary}
                size={ButtonSizeTypes.Small}
                variant={ButtonVariantTypes.Contained}
                testId="statusUpdate"
                label={t('label.updateStatus')}
                onClick={() => updateStatus()}
                toolTip={t('toolTip.button.statusUpdate')}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={5}>
              {showElement ? renderMessageResponce() : ''}
            </Grid>
          </Grid>
          {data && Array.isArray(data) && data.length > 0 && <StatusHistoryTableList data={data} />}
        </DialogContent>
        <DialogActions>
          <Button
            color={ButtonColorTypes.Secondary}
            variant={ButtonVariantTypes.Contained}
            size={ButtonSizeTypes.Small}
            testId="statusClose"
            label={t('label.close')}
            onClick={handleClose}
            toolTip={t('label.close')}
          />
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ManageEntityStatus;
