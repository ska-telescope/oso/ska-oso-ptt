/* eslint-disable no-restricted-syntax */
import React from 'react';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { mount } from 'cypress/react18';
import { THEME_DARK, THEME_LIGHT } from '@ska-telescope/ska-gui-components';
import theme from '../../../../services/theme/theme';
import StatusHistoryTableList from './StatusHistoryTableList';
import statusHistory from '../../../../DataModels/DataFiles/statusMock';

const THEME = [THEME_DARK, THEME_LIGHT];

describe('<SBDTableList />', () => {
  for (const theTheme of THEME) {
    it(`Theme ${theTheme}: Renders SBDTableList`, () => {
      mount(
        <ThemeProvider theme={theme(theTheme)}>
          <CssBaseline />
          <StatusHistoryTableList data={statusHistory} />
        </ThemeProvider>
      );
    });
  }
});
