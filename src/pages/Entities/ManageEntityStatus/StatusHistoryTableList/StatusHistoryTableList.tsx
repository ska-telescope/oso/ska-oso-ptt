import { Box } from '@mui/material';
import { DataGrid } from '@ska-telescope/ska-gui-components';
import React from 'react';
import { useTranslation } from 'react-i18next';
import StatusDataModel from '../../../../DataModels/Models/StatusBlockModel';
import { toUTCDateTimeFormat } from '../../../../utils/constants';

interface EntryFieldProps {
  data: StatusDataModel[];
}

const StatusHistoryTableList = ({ data }: EntryFieldProps) => {
  const { t } = useTranslation('translations');
  let id = 1;
  data &&
    data.length > 0 &&
    data.map((row) => {
      row.id = id++;
      return row;
    });
  const columns = [
    {
      field: 'id',
      headerName: '#',
      width: 80
    },
    {
      field: 'current_status',
      headerName: t('label.currentStatus'),
      width: 200,
      renderCell: (params) => params.row.current_status
    },
    {
      field: 'last_modified_on',
      headerName: t('label.lastModifiedOn'),
      width: 200,
      renderCell: (params) =>
        toUTCDateTimeFormat(params.row.last_modified_on || params.row.metadata.last_modified_on)
    },
    {
      field: 'last_modified_by',
      headerName: t('label.lastModifiedBy'),
      width: 150,
      renderCell: (params) => params.row.metadata.last_modified_by
    }
  ];
  return (
    <Box data-testid="availableData">
      <div>
        <span data-testid="msgStatusHistory">{t('msg.statusHistory')}</span>
      </div>
      <DataGrid
        ariaDescription={t('ariaLabel.gridTableDescription')}
        ariaTitle={t('ariaLabel.gridTable')}
        columns={columns}
        rows={data}
        testId="statusHistoryTable"
      />
    </Box>
  );
};

export default StatusHistoryTableList;
