const statusHistory = [
  {
    id: 1,
    current_status: 'created',
    eb_ref: 'eb-mvp01-20220923-00001',
    metadata: {
      created_by: 'DefaultUser',
      created_on: '2024-04-14T11:51:16.367278+05:30',
      last_modified_by: 'DefaultUser',
      last_modified_on: '2024-04-14T11:51:16.367278+05:30',
      version: 1
    },
    previous_status: 'created'
  }
];

export default statusHistory;
