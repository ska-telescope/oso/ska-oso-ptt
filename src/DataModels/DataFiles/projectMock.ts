const projectDataList = [
  {
    interface: 'https://schema.skao.int/ska-oso-pdm-prj/0.1',
    prj_id: 'prj-mvp01-20220923-00001',
    telescope: 'ska_mid',
    id: 1,
    metadata: {
      version: 1,
      created_by: 'DefaultUser',
      created_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_by: 'DefaultUser'
    },
    obs_blocks: [
      {
        name: 'Block 1',
        obs_block_id: 'ob-1',
        sbd_ids: ['sbd-t0001-20241007-00003', 'sbd-t0001-20241007-00005']
      },
      {
        name: 'Block 2',
        obs_block_id: 'ob-2',
        sbd_ids: ['sbd-t0001-20241007-00004']
      }
    ],
    author: {
      pis: ['John Lennon'],
      cois: ['Ringo Starr', 'George Harrison']
    }
  },
  {
    interface: 'https://schema.skao.int/ska-oso-pdm-prj/0.1',
    prj_id: 'prj-mvp01-20220923-00001',
    telescope: 'ska_mid',
    id: 2,
    metadata: {
      version: 1,
      created_by: 'DefaultUser',
      created_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_by: 'DefaultUser'
    },
    obs_blocks: [
      {
        name: 'Block 1',
        obs_block_id: 'ob-1',
        sbd_ids: [
          'sbd-mvp01-20220923-00001',
          'sbd-mvp01-20220923-00002',
          'sbd-mvp01-20220923-00003'
        ]
      },
      {
        name: 'Block 2',
        obs_block_id: 'ob-2',
        sbd_ids: [
          'sbd-mvp01-20220923-00004',
          'sbd-mvp01-20220923-00005',
          'sbd-mvp01-20220923-00006'
        ]
      }
    ],
    author: {
      pis: ['John Lennon'],
      cois: ['Ringo Starr', 'George Harrison']
    }
  },
  {
    interface: 'https://schema.skao.int/ska-oso-pdm-prj/0.1',
    prj_id: 'prj-mvp01-20220923-00001',
    telescope: 'ska_mid',
    id: 3,
    metadata: {
      version: 1,
      created_by: 'DefaultUser',
      created_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_by: 'DefaultUser'
    },
    obs_blocks: [
      {
        name: 'Block 1',
        obs_block_id: 'ob-1',
        sbd_ids: [
          'sbd-mvp01-20220923-00001',
          'sbd-mvp01-20220923-00002',
          'sbd-mvp01-20220923-00003'
        ]
      },
      {
        name: 'Block 2',
        obs_block_id: 'ob-2',
        sbd_ids: [
          'sbd-mvp01-20220923-00004',
          'sbd-mvp01-20220923-00005',
          'sbd-mvp01-20220923-00006'
        ]
      }
    ],
    author: {
      pis: ['John Lennon'],
      cois: ['Ringo Starr', 'George Harrison']
    }
  }
];

export default projectDataList;
