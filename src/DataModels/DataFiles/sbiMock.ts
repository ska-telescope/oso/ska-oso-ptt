const sbiDataList = [
  {
    metadata: {
      version: 1,
      created_by: 'DefaultUser',
      created_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_by: 'DefaultUser'
    },
    interface: 'https://schema.skao.int/ska-oso-pdm-sbi/0.1',
    sbi_id: 'sbi-mvp01-20220923-00001',
    id: 1,
    sbd_ref: 'sbd-mvp01-20220923-00001',
    tab_oda_sbd_info: {
      sbd_id: 'sbd-mvp01-20220923-00001'
    },
    sbd_version: 1,
    eb_ref: 'eb-mvp01-20220923-00001',
    telescope: 'ska_mid',
    subarray_id: 3,
    activities: [
      {
        activity_ref: 'test activity',
        executed_at: '2022-09-23T15:43:53.971548+00:00',
        runtime_args: [
          {
            function_name: 'test_fn',
            function_args: {
              kwargs: {
                foo: 'bar'
              }
            }
          }
        ]
      }
    ]
  },
  {
    metadata: {
      version: 1,
      created_by: 'DefaultUser',
      created_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_by: 'DefaultUser'
    },
    interface: 'https://schema.skao.int/ska-oso-pdm-sbi/0.1',
    sbi_id: 'sbi-mvp01-20220923-00002',
    id: 2,
    sbd_ref: 'sbd-mvp01-20220923-00002',
    tab_oda_sbd_info: {
      sbd_id: 'sbd-mvp01-20220923-00002'
    },
    sbd_version: 1,
    eb_ref: 'eb-mvp01-20220923-00002',
    telescope: 'ska_mid',
    subarray_id: 3,
    activities: [
      {
        activity_ref: 'test activity',
        executed_at: '2022-09-23T15:43:53.971548+00:00',
        runtime_args: [
          {
            function_name: 'test_fn',
            function_args: {
              kwargs: {
                foo: 'bar'
              }
            }
          }
        ]
      }
    ]
  },
  {
    metadata: {
      version: 1,
      created_by: 'DefaultUser',
      created_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_by: 'DefaultUser'
    },
    interface: 'https://schema.skao.int/ska-oso-pdm-sbi/0.1',
    sbi_id: 'sbi-mvp01-20220923-00003',
    id: 3,
    sbd_ref: 'sbd-mvp01-20220923-00003',
    tab_oda_sbd_info: {
      sbd_id: 'sbd-mvp01-20220923-00003'
    },
    sbd_version: 1,
    eb_ref: 'eb-mvp01-20220923-00003',
    telescope: 'ska_mid',
    subarray_id: 3,
    activities: [
      {
        activity_ref: 'test activity',
        executed_at: '2022-09-23T15:43:53.971548+00:00',
        runtime_args: [
          {
            function_name: 'test_fn',
            function_args: {
              kwargs: {
                foo: 'bar'
              }
            }
          }
        ]
      }
    ]
  },
  {
    metadata: {
      version: 1,
      created_by: 'DefaultUser',
      created_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_on: '2022-03-28T15:43:53.971548+00:00',
      last_modified_by: 'DefaultUser'
    },
    interface: 'https://schema.skao.int/ska-oso-pdm-sbi/0.1',
    sbi_id: 'sbi-mvp01-20220923-00004',
    id: 4,
    sbd_ref: 'sbd-mvp01-20220923-00004',
    tab_oda_sbd_info: {
      sbd_id: 'sbd-mvp01-20220923-00004'
    },
    sbd_version: 1,
    eb_ref: 'eb-mvp01-20220923-00004',
    telescope: 'ska_mid',
    subarray_id: 3,
    activities: [
      {
        activity_ref: 'test activity',
        executed_at: '2022-09-23T15:43:53.971548+00:00',
        runtime_args: [
          {
            function_name: 'test_fn',
            function_args: {
              kwargs: {
                foo: 'bar'
              }
            }
          }
        ]
      }
    ]
  }
];

export default sbiDataList;
