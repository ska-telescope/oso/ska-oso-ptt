const ebDataList = [
  {
    created_by: 'DefaultUser',
    created_on: '2023-08-10T11:08:28.234000Z',
    eb_id: 'eb-mvp01-20220923-00002',
    sbd_ref: 'sbd-mvp01-20220923-00001',
    sbi_ref: 'sbi-mvp01-20220923-00001',
    id: 1,
    info: {
      eb_id: 'eb-mvp01-20220923-00002',
      interface: 'https://schema.skao.int/ska-oso-pdm-eb/0.1',
      metadata: {
        created_by: 'DefaultUser',
        created_on: '2023-07-24T15:44:55.268078+00:00',
        last_modified_by: 'DefaultUser',
        last_modified_on: '2023-07-24T15:44:55.268078+00:00',
        version: 1
      },
      request_responses: [
        {
          request: 'ska_oso_scripting.functions.devicecontrol.release_all_resources',
          request_args: "{'args': None, 'kwargs': {'subarray_id': '1'}}",
          request_sent_at: '2022-09-23T15:43:53.971548+00:00',
          response: {
            result: 'this is a result'
          },
          response_received_at: '2022-09-23T15:43:53.971548+00:00',
          status: 'OK'
        },
        {
          error: {
            detail: 'this is an error'
          },
          request: 'ska_oso_scripting.functions.devicecontrol.scan',
          request_sent_at: '2022-09-23T15:43:53.971548+00:00',
          status: 'ERROR'
        }
      ],
      sbd_id: 'sbd-mvp01-20200325-00002',
      sbd_version: 1,
      telescope: 'ska_mid'
    },
    last_modified_by: 'DefaultUser',
    last_modified_on: '2023-08-10T11:08:28.234000Z',
    sbd_id: 'sbd-mvp01-20200325-00002',
    sbd_version: 1,
    version: 1
  },
  {
    created_by: 'DefaultUser',
    created_on: '2023-08-06T10:56:15.781000Z',
    eb_id: 'eb-mvp01-20220923-00003',
    sbd_ref: 'sbd-mvp01-20220923-00001',
    sbi_ref: 'sbi-mvp01-20220923-00001',
    id: 2,
    info: {
      eb_id: 'eb-mvp01-20220923-00003',
      interface: 'https://schema.skao.int/ska-oso-pdm-eb/0.1',
      metadata: {
        created_by: 'DefaultUser',
        created_on: '2023-07-26T10:56:15.781498+00:00',
        last_modified_by: 'DefaultUser',
        last_modified_on: '2023-07-26T10:56:15.781498+00:00',
        version: 1
      },
      request_responses: [
        {
          request: 'ska_oso_scripting.functions.devicecontrol.release_all_resources',
          request_args: "{'args': None, 'kwargs': {'subarray_id': '1'}}",
          request_sent_at: '2022-09-23T15:43:53.971548+00:00',
          response: {
            result: 'this is a result'
          },
          response_received_at: '2022-09-23T15:43:53.971548+00:00',
          status: 'OK'
        },
        {
          error: {
            detail: 'this is an error'
          },
          request: 'ska_oso_scripting.functions.devicecontrol.scan',
          request_sent_at: '2022-09-23T15:43:53.971548+00:00',
          status: 'ERROR'
        }
      ],
      sbd_id: 'sbd-mvp01-20200325-00001',
      sbd_version: 1,
      telescope: 'ska_mid'
    },
    last_modified_by: 'DefaultUser',
    last_modified_on: '2023-08-06T10:56:15.781000Z',
    sbd_id: 'sbd-mvp01-20200325-00005',
    sbd_version: 1,
    version: 1
  },
  {
    created_by: 'DefaultUser',
    created_on: '2023-08-10T11:08:29.810000Z',
    eb_id: 'eb-mvp01-20220923-00004',
    sbd_ref: 'sbd-mvp01-20220923-00001',
    sbi_ref: 'sbi-mvp01-20220923-00001',
    id: 3,
    info: {
      eb_id: 'eb-mvp01-20220923-00004',
      interface: 'https://schema.skao.int/ska-oso-pdm-eb/0.1',
      metadata: {
        created_by: 'DefaultUser',
        created_on: '2023-07-26T11:08:29.810555+00:00',
        last_modified_by: 'DefaultUser',
        last_modified_on: '2023-07-26T11:08:29.810555+00:00',
        version: 1
      },
      request_responses: [
        {
          request: 'ska_oso_scripting.functions.devicecontrol.release_all_resources',
          request_args: "{'args': None, 'kwargs': {'subarray_id': '1'}}",
          request_sent_at: '2022-09-23T15:43:53.971548+00:00',
          response: {
            result: 'this is a result'
          },
          response_received_at: '2022-09-23T15:43:53.971548+00:00',
          status: 'OK'
        },
        {
          request: 'ska_oso_scripting.functions.devicecontrol.assign_all_resources',
          request_args: "{'args': None, 'kwargs': {'subarray_id': '1'}}",
          request_sent_at: '2022-09-23T15:43:53.971548+00:00',
          response: {
            result: 'this is a result'
          },
          response_received_at: '2022-09-23T15:43:53.971548+00:00',
          status: 'OK'
        },
        {
          error: {
            detail: 'this is an error'
          },
          request: 'ska_oso_scripting.functions.devicecontrol.scan',
          request_sent_at: '2022-09-23T15:43:53.971548+00:00',
          status: 'ERROR'
        }
      ],
      sbd_id: 'sbd-mvp01-20200325-00001',
      sbd_version: 1,
      telescope: 'ska_mid'
    },
    last_modified_by: 'DefaultUser',
    last_modified_on: '2023-08-10T11:08:29.810000Z',
    sbd_id: 'sbd-mvp01-20200325-00001',
    sbd_version: 1,
    version: 1
  },
  {
    created_by: 'DefaultUser',
    created_on: '2023-08-17T11:54:31.584000Z',
    eb_id: 'eb-mvp01-20220923-00010',
    sbd_ref: 'sbd-mvp01-20220923-00001',
    sbi_ref: 'sbi-mvp01-20220923-00001',
    id: 4,
    info: {
      eb_id: 'eb-mvp01-20220923-00010',
      interface: 'https://schema.skao.int/ska-oso-pdm-eb/0.1',
      metadata: {
        created_by: 'DefaultUser',
        created_on: '2023-08-16T11:54:31.584199+00:00',
        last_modified_by: 'DefaultUser',
        last_modified_on: '2023-08-16T11:54:31.584199+00:00',
        version: 1
      },
      request_responses: [
        {
          request: 'ska_oso_scripting.functions.devicecontrol.release_all_resources',
          request_args: "{'args': None, 'kwargs': {'subarray_id': '1'}}",
          request_sent_at: '2022-09-23T15:43:53.971548+00:00',
          response: {
            result: 'this is a result'
          },
          response_received_at: '2022-09-23T15:43:53.971548+00:00',
          status: 'OK'
        },
        {
          request: 'ska_oso_scripting.functions.devicecontrol.assign_all_resources',
          request_args: "{'args': None, 'kwargs': {'subarray_id': '1'}}",
          request_sent_at: '2022-09-23T15:43:53.971548+00:00',
          response: {
            result: 'this is a result'
          },
          response_received_at: '2022-09-23T15:43:53.971548+00:00',
          status: 'OK'
        },
        {
          error: {
            detail: 'this is an error'
          },
          request: 'ska_oso_scripting.functions.devicecontrol.scan',
          request_sent_at: '2022-09-23T15:43:53.971548+00:00',
          status: 'ERROR'
        }
      ],
      sbd_id: 'sbd-mvp01-20200325-00001',
      sbd_version: 1,
      telescope: 'ska_mid'
    },
    last_modified_by: 'DefaultUser',
    last_modified_on: '2023-08-16T11:54:31.584199Z',
    sbd_id: 'sbd-mvp01-20200325-00004',
    sbd_version: 1,
    version: 1
  },
  {
    created_by: 'DefaultUser',
    created_on: '2023-08-17T11:54:22.456000Z',
    eb_id: 'eb-mvp01-20220923-00009',
    sbd_ref: 'sbd-mvp01-20220923-00001',
    sbi_ref: 'sbi-mvp01-20220923-00001',
    id: 5,
    info: {
      eb_id: 'eb-mvp01-20220923-00009',
      interface: 'https://schema.skao.int/ska-oso-pdm-eb/0.1',
      metadata: {
        created_by: 'DefaultUser',
        created_on: '2023-08-16T11:54:22.456355+00:00',
        last_modified_by: 'DefaultUser',
        last_modified_on: '2023-08-16T11:54:22.456355+00:00',
        version: 1
      },
      request_responses: [
        {
          request: 'ska_oso_scripting.functions.devicecontrol.release_all_resources',
          request_args: "{'args': None, 'kwargs': {'subarray_id': '1'}}",
          request_sent_at: '2022-09-23T15:43:53.971548+00:00',
          response: {
            result: 'this is a result'
          },
          response_received_at: '2022-09-23T15:43:53.971548+00:00',
          status: 'OK'
        },
        {
          request: 'ska_oso_scripting.functions.devicecontrol.assign_all_resources',
          request_args: "{'args': None, 'kwargs': {'subarray_id': '1'}}",
          request_sent_at: '2022-09-23T15:43:53.971548+00:00',
          response: {
            result: 'this is a result'
          },
          response_received_at: '2022-09-23T15:43:53.971548+00:00',
          status: 'OK'
        },
        {
          error: {
            detail: 'this is an error'
          },
          request: 'ska_oso_scripting.functions.devicecontrol.scan',
          request_sent_at: '2022-09-23T15:43:53.971548+00:00',
          status: 'ERROR'
        }
      ],
      sbd_id: 'sbd-mvp01-20200325-00001',
      sbd_version: 1,
      telescope: 'ska_mid'
    },
    last_modified_by: 'DefaultUser',
    last_modified_on: '2023-08-16T11:54:22.456355Z',
    sbd_id: 'sbd-mvp01-20200325-00001',
    sbd_version: 1,
    version: 1
  }
];

export default ebDataList;
