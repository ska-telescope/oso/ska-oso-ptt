interface Metadata {
  version: number;
  created_by: string;
  created_on: string;
  last_modified_on: string;
  last_modified_by: string;
}

interface RuntimeArgs {
  function_name: string;
  function_args: {
    kwargs: {
      [key: string]: string;
    };
  };
}

interface Activity {
  activity_ref: string;
  executed_at: string;
  runtime_args: RuntimeArgs[];
}

interface TabOdaSbdInfo {
  sbd_id: string;
}

export default interface SBIDataModel {
  metadata: Metadata;
  interface: string;
  sbi_id: string;
  id: number;
  sbd_ref: string;
  tab_oda_sbd_info: TabOdaSbdInfo;
  sbd_version: number;
  eb_ref: string;
  telescope: string;
  subarray_id: number;
  activities: Activity[];
}
