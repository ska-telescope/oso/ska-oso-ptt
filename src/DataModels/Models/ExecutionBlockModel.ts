interface Metadata {
  created_by: string;
  created_on: string;
  last_modified_by: string;
  last_modified_on: string;
  version: number;
}

interface RequestResponse {
  request: string;
  request_args?: string;
  request_sent_at: string;
  response?: {
    result: string;
  };
  response_received_at?: string;
  status: string;
  error?: {
    detail: string;
  };
}

interface EBInfo {
  eb_id: string;
  interface: string;
  metadata: Metadata;
  request_responses: RequestResponse[];
  sbd_id: string;
  sbd_version: number;
}

export default interface EBDataModel {
  created_by: string;
  eb_id: string;
  sbd_ref: string;
  sbi_ref: string;
  id: number;
  info: EBInfo;
  last_modified_by: string;
  last_modified_on: string;
  sbd_id: string;
  sbd_version: number;
}
