interface FunctionArgs {
  init: {
    args: string[];
    kwargs: {
      [key: string]: string;
    };
  };
  main: {
    args: string[];
    kwargs: {
      [key: string]: string;
    };
  };
}

interface Activities {
  allocate: {
    function_args: FunctionArgs;
    kind: string;
    path: string;
  };
  observe: {
    branch: string;
    function_args: FunctionArgs;
    kind: string;
    path: string;
    repo: string;
  };
}

interface Fsp {
  channel_averaging_map?: number[][];
  channel_offset?: number;
  frequency_slice_id: number;
  fsp_id: number;
  function_mode: string;
  integration_factor: number;
  output_link_map?: number[][];
  zoom_factor: number;
  zoom_window_tuning?: number; // Add optional property
}

interface DishAllocations {
  receptor_ids: string[];
}

interface DishConfigurations {
  dish_configuration_id: string;
  receiver_band: string;
}

interface Subarray {
  subarray_name: string;
}

interface CspConfigurations {
  cbf: {
    fsp: Fsp[];
  };
  common: {
    band_5_tuning: number[];
    subarray_id: number;
  };
  config_id: string;
  subarray: Subarray;
}

interface SpectralWindow {
  count: number;
  freq_max: number;
  freq_min: number;
  link_map?: number[][];
  spectral_window_id: string;
  start: number;
  stride?: number;
}

interface Channels {
  channels_id: string;
  spectral_windows: SpectralWindow[]; // Update property name
}

interface Beams {
  beam_id: string;
  function?: string;
  search_beam_id?: number;
  timing_beam_id?: number;
  vlbi_beam_id?: number;
}

interface Polarisations {
  corr_type: string[];
  polarisations_id: string;
}

interface ScanTypes {
  beams: Beams[];
  channels_id?: string;
  field_id?: string;
  polarisations_id?: string;
  scan_type_id: string; // Update property name
  derive_from?: string;
}

interface ExecutionBlock {
  beams: Beams[];
  channels: Channels[];
  context: {
    [key: string]: string | number;
  };
  eb_id: string;
  max_length: number;
  polarisations: Polarisations[];
  scan_types: ScanTypes[];
}

interface Script {
  kind: string;
  name: string;
  version: string;
}

interface Resources {
  csp_links: number[];
  receive_nodes: number;
  receptors: string[];
}

interface Script {
  kind: string;
  name: string;
  version: string;
}

interface ProcessingBlock {
  dependencies?: {
    kind: string[];
    pb_id: string;
  }[];
  parameters: {
    [key: string]: string;
  };
  pb_id: string;
  sbi_ids: string[];
  script: Script;
}

interface Resources {
  csp_links: number[];
  receive_nodes: number;
  receptors: string[];
}

interface SdpConfigurations {
  execution_block: ExecutionBlock;
  processing_blocks: ProcessingBlock[];
  resources: Resources;
}

interface Parameter {
  kind: string;
  offset_arcsec?: number;
  n_rows?: number;
  pa?: number;
  row_length_arcsec?: number;
  row_offset_arcsec?: number;
  row_offset_angle?: number;
  unidirectional?: boolean;
}
interface Target {
  pointing_pattern: {
    active: string;
    parameters: Parameter[];
  };
  reference_coordinate: {
    dec: string;
    kind: string;
    ra: string;
    reference_frame: string;
    unit: string[];
  };
  target_id: string;
}

interface ScanDefinitions {
  csp_configuration: string;
  dish_configuration: string;
  scan_definition_id: string;
  scan_duration_ms: number;
  scan_type: string;
  target: string;
}

export default interface SBDDataModel {
  created_by: string;
  created_on: string;
  id: number;
  sbd_id: string;
  info: {
    activities: Activities;
    csp_configurations: CspConfigurations[];
    dish_allocations: DishAllocations;
    dish_configurations: DishConfigurations[];
    sdp_configuration: SdpConfigurations;
    scan_sequence: string[];
    interface: string;
    targets: Target[];
    scan_definitions: ScanDefinitions[];
    metadata: {
      created_by: string;
      created_on: string;
      last_modified_by: string;
      last_modified_on: string;
      version: number;
    };
  };
}
