interface Metadata {
  created_by: string;
  created_on: string;
  last_modified_by: string;
  last_modified_on: string;
  version: number;
}

export default interface StatusDataModel {
  id: number;
  previous_status: string;
  current_status: string;
  metadata: Metadata;
  eb_ref?: string;
  sbd_ref?: string;
  sbi_ref?: string;
}
