interface Author {
  cois: string[];
  pis: string[];
}

interface Metadata {
  created_by: string;
  created_on: string;
  last_modified_by: string;
  last_modified_on: string;
  version: number;
}

interface ObsBlock {
  name: string;
  obs_block_id: string;
  sbd_ids: string[];
}

export default interface ProjectDataModel {
  id: number;
  author: Author;
  interface: string;
  metadata: Metadata;
  obs_blocks: ObsBlock[];
  prj_id: string;
  telescope: string;
}
