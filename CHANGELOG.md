Changelog
===========

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

1.7.1
-------

- Fixed Display UI SBD Page issue.

1.7.0
-------

- Added status entity apis to display status.
- Updated PTT UI to consume ODA version 7.1.0


1.6.0
-------

- Improved code coverage for component level test.
- Added CHANGELOG file to the repository and RTD.
- Updated RTD documentation to include user guide for Project Tracking Tool.

1.5.0
------

- Added support for PTT backend services


1.4.0
------


- Added support for project status entity and updated status functionality

1.3.1
------

- Updated make modules in order to fix umbrella charts

1.3.0
-----

- Added support for SBD,SBI and EB status entity and updated status functionality

1.2.0
-----

- Added helm chart ssuport

1.1.0
-----

- Added PTT UI with SBI support.

1.0.0
-----

- Added PTT UI new repository with SBD and ED support
