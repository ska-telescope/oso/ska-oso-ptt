/* eslint-disable cypress/no-unnecessary-waiting */
import moment from 'moment';
import enTranslations from '../../../public/locales/en/translations.json';

const language = 'English';
const waitTime = 2000;
const startDate = moment().utc().subtract(300, 'days').format('YYYY-MM-DD');
const endDate = moment().utc().format('YYYY-MM-DD');
const invalidDate = moment().utc().add(1, 'days').format('YYYY-MM-DD');
const invalidStartDate = moment().utc().subtract(5, 'days').format('YYYY-MM-DD');
const invalidEndDate = moment().utc().subtract(10, 'days').format('YYYY-MM-DD');

let translation;
if (language === 'English') {
  translation = enTranslations;
}

const LOCAL_HOST = 'http://localhost:8090/';
context('Project Tracking Tool', () => {
  const validateEbDataTable = () => {
    cy.wait(waitTime);
    cy.get('[data-testid="content"]').then((ele) => {
      if (ele.find('[data-testid="ebTable"]').length > 0) {
        cy.get('[data-testid="ebTable"]').should('be.visible');
        cy.get('[data-testid="ebTable"]')
          .get('[data-field="eb_id"]')
          .contains(translation.label.ebId);
        cy.get('[data-testid="ebTable"]')
          .get('[data-field="sbd_ref"]')
          .contains(translation.label.sbdId);
        cy.get('[data-testid="ebTable"]')
          .get('[data-field="created_on"]')
          .contains(translation.label.createdOn);
        cy.get('[data-testid="ebTable"]')
          .get('[data-field="created_by"]')
          .contains(translation.label.createdBy);
        cy.get('[data-testid="ebTable"]')
          .get('[data-field="status"]')
          .contains(translation.label.currentStatus);
        cy.get('body').then((element) => {
          if (element.find('[data-testid="manageEntityStatus"]').length) {
            cy.get('[data-testid="manageEntityStatus"]').click({ force: true, multiple: true });
            cy.wait(waitTime);
            cy.get('[data-testid="dialogStatus"]').should('be.visible');
            cy.get('[data-testid="statusDialogTitle"]').should('be.visible');
            cy.get('[data-testid="currentStatus"]').should('be.visible');
            cy.get('[data-testid="statusClose"]').should('be.visible');
          }
        });
      }
    });
  };

  const validateSbdDataTable = () => {
    cy.wait(waitTime);
    cy.get('[data-testid="content"]').then((ele) => {
      if (ele.find('[data-testid="sbdTable"]').length > 0) {
        cy.get('[data-testid="sbdTable"]').should('be.visible');
        cy.get('[data-testid="sbdTable"]')
          .get('[data-field="sbd_id"]')
          .contains(translation.label.sbdId);
        cy.get('[data-testid="sbdTable"]')
          .get('[data-field="created_on"]')
          .contains(translation.label.createdOn);
        cy.get('[data-testid="sbdTable"]')
          .get('[data-field="created_by"]')
          .contains(translation.label.createdBy);
        cy.get('[data-testid="sbdTable"]').get('[data-field="info"]').contains('More Info');
        cy.get('body').then((element) => {
          if (element.find('[data-testid="iconViewSBD"]').length) {
            cy.get('[data-testid="iconViewSBD"]').click({ force: true, multiple: true });
            cy.wait(waitTime);
            cy.get('[data-testid="sbdDialogTitle"]').contains(translation.label.sbdInfo);
          }
        });
        cy.get('[data-testid="sbdTable"]')
          .get('[data-field="status"]')
          .contains(translation.label.currentStatus);
        cy.get('body').then((element) => {
          if (element.find('[data-testid="manageEntityStatus"]').length) {
            cy.get('[data-testid="manageEntityStatus"]').click({ force: true, multiple: true });
            cy.wait(waitTime);
            cy.get('[data-testid="dialogStatus"]').should('be.visible');
            cy.get('[data-testid="statusDialogTitle"]').should('be.visible');
            cy.get('[data-testid="currentStatus"]').should('be.visible');
            cy.get('[data-testid="statusClose"]').should('be.visible');
          }
        });
      }
    });
  };

  const validateSbiDataTable = () => {
    cy.wait(waitTime);
    cy.get('[data-testid="content"]').then((ele) => {
      if (ele.find('[data-testid="sbiTable"]').length > 0) {
        cy.get('[data-testid="sbiTable"]').should('be.visible');
        cy.get('[data-testid="sbiTable"]')
          .get('[data-field="sbi_id"]')
          .contains(translation.label.sbiId);
        cy.get('[data-testid="sbiTable"]')
          .get('[data-field="sbd_ref"]')
          .contains(translation.label.sbdId);
        cy.get('[data-testid="sbiTable"]')
          .get('[data-field="eb_ref"]')
          .contains(translation.label.ebId);
        cy.get('[data-testid="sbiTable"]')
          .get('[data-field="created_on"]')
          .contains(translation.label.createdOn);
        cy.get('[data-testid="sbiTable"]')
          .get('[data-field="created_by"]')
          .contains(translation.label.createdBy);
        cy.get('[data-testid="sbiTable"]')
          .get('[data-field="status"]')
          .contains(translation.label.currentStatus);
        cy.get('body').then((element) => {
          if (element.find('[data-testid="manageEntityStatus"]').length) {
            cy.get('[data-testid="manageEntityStatus"]').click({ force: true, multiple: true });
            cy.wait(waitTime);
            cy.get('[data-testid="dialogStatus"]').should('be.visible');
            cy.get('[data-testid="statusDialogTitle"]').should('be.visible');
            cy.get('[data-testid="currentStatus"]').should('be.visible');
            cy.get('[data-testid="statusClose"]').should('be.visible');
          }
        });
      }
    });
  };

  const validateProjectDataTable = () => {
    cy.wait(waitTime);
    cy.get('body').then((ele) => {
      if (ele.find('[data-testid="projectTable"]').length > 0) {
        cy.get('[data-testid="projectTable"]').should('be.visible');
        cy.get('[data-testid="projectTable"]')
          .get('[data-field="prj_id"]')
          .contains(translation.label.prjId);
        cy.get('[data-testid="projectTable"]')
          .get('[data-field="created_on"]')
          .contains(translation.label.createdOn);
        cy.get('[data-testid="projectTable"]')
          .get('[data-field="created_by"]')
          .contains(translation.label.createdBy);
        cy.get('[data-testid="projectTable"]')
          .get('[data-field="obs_blocks"]')
          .contains(translation.label.obsblocks);
        cy.get('[data-testid="projectTable"]')
          .get('[data-field="status"]')
          .contains(translation.label.currentStatus);
        cy.get('body').then((element) => {
          if (element.find('[data-testid="manageEntityStatus"]').length) {
            cy.get('[data-testid="manageEntityStatus"]').click({ force: true, multiple: true });
            cy.wait(waitTime);
            cy.get('[data-testid="dialogStatus"]').should('be.visible');
            cy.get('[data-testid="statusDialogTitle"]').should('be.visible');
            cy.get('[data-testid="currentStatus"]').should('be.visible');
            cy.get('[data-testid="msgStatusHistory"]').contains(translation.msg.statusHistory);
            cy.get('[data-testid="statusClose"]').should('be.visible');
          }
        });
      }
    });
  };

  beforeEach(() => {
    cy.visit(LOCAL_HOST);
  });

  it('Verify light/dark mode is available', () => {
    cy.get('[data-testid="Brightness7Icon"]').click();
    cy.get('[data-testid="Brightness4Icon"]').should('be.visible');
  });

  it('verify SBI for records for last 7 days', () => {
    cy.get('[data-testid="sbiEntity"]').click();
    cy.get('[data-testid="last7days"]').click();
    cy.get('[data-testid="last7days"]').contains(translation.label.last7days);
    cy.get('[data-testid="last7days"]').click();
    cy.get('[data-testid="msgLastWeek"]').contains('Showing records for last 7 days');
    validateSbiDataTable();
  });

  it('verify SBI for records for today', () => {
    cy.get('[data-testid="sbiEntity"]').click();
    cy.get('[data-testid="today"]').contains(translation.label.today);
    cy.get('[data-testid="today"]').click();
    cy.get('[data-testid="msgToday"]').contains(translation.msg.today);
    validateSbiDataTable();
  });

  it('verify SBI for records for selected dates', () => {
    cy.get('[data-testid="sbiEntity"]').click();
    cy.get('[data-testid="dateEntryStart"]').type(startDate);
    cy.get('[data-testid="dateEntryEnd"]').type(endDate);
    cy.get('[data-testid="dateSearch"]').click();
    validateSbiDataTable();
  });

  it('verify SBI, invalid start date selected', () => {
    cy.get('[data-testid="sbiEntity"]').click();
    cy.get('[data-testid="dateEntryStart"]').type(invalidDate);
    cy.get('.MuiFormHelperText-root').contains(translation.msg.errFutureDate);
  });

  it('verify SBI, invalid end date selected', () => {
    cy.get('[data-testid="sbiEntity"]').click();
    cy.get('[data-testid="dateEntryEnd"]').type(invalidDate);
    cy.get('.MuiFormHelperText-root').contains(translation.msg.errFutureDate);
  });

  it('verify SBI, start date greater than end date selected', () => {
    cy.get('[data-testid="sbiEntity"]').click();
    cy.get('[data-testid="dateEntryStart"]').type(invalidStartDate);
    cy.get('[data-testid="dateEntryEnd"]').type(invalidEndDate);
    cy.get('.MuiFormHelperText-root').contains(translation.msg.errInvalidEndDate);
  });

  it('verify SBI Id for empty SBI Id', () => {
    cy.get('[data-testid="sbiEntity"]').click();
    cy.get('.MuiFormHelperText-root').contains(translation.msg.requiredEntityId);
  });

  it('verify SBI for records by SBI Id pattern', () => {
    cy.get('[data-testid="sbiEntity"]').click();
    cy.get('[data-testid="searchId"]').type('sbi-mvp01');
    cy.get('[data-testid="idSearch"]').click();
    validateSbiDataTable();
  });

  it('verify SBD for records for last 7 days', () => {
    cy.get('[data-testid="sbdEntity"]').click();
    cy.get('[data-testid="last7days"]').contains(translation.label.last7days);
    cy.get('[data-testid="last7days"]').click();
    cy.get('[data-testid="msgLastWeek"]').contains(translation.msg.last7days);
    validateSbdDataTable();
  });

  it('verify SBD for records for today', () => {
    cy.get('[data-testid="sbdEntity"]').click();
    cy.get('[data-testid="today"]').contains(translation.label.today);
    cy.get('[data-testid="today"]').click();
    cy.get('[data-testid="msgToday"]').contains(translation.msg.today);
    validateSbdDataTable();
  });

  it('verify SBD for records for selected dates', () => {
    cy.get('[data-testid="sbdEntity"]').click();
    cy.get('[data-testid="dateEntryStart"]').type(startDate);
    cy.get('[data-testid="dateEntryEnd"]').type(endDate);
    cy.get('[data-testid="dateSearch"]').click();
    validateSbdDataTable();
  });

  it('verify SBD, invalid start date selected', () => {
    cy.get('[data-testid="sbdEntity"]').click();
    cy.get('[data-testid="dateEntryStart"]').type(invalidDate);
    cy.get('.MuiFormHelperText-root').contains(translation.msg.errFutureDate);
  });

  it('verify SBD, invalid end date selected', () => {
    cy.get('[data-testid="sbdEntity"]').click();
    cy.get('[data-testid="dateEntryEnd"]').type(invalidDate);
    cy.get('.MuiFormHelperText-root').contains(translation.msg.errFutureDate);
  });

  it('verify SBD, start date greater than end date selected', () => {
    cy.get('[data-testid="sbdEntity"]').click();
    cy.get('[data-testid="dateEntryStart"]').type(invalidStartDate);
    cy.get('[data-testid="dateEntryEnd"]').type(invalidEndDate);
    cy.get('.MuiFormHelperText-root').contains(translation.msg.errInvalidEndDate);
  });

  it('verify SBD Id for empty SBD Id', () => {
    cy.get('[data-testid="sbdEntity"]').click();
    cy.get('.MuiFormHelperText-root').contains(translation.msg.requiredEntityId);
  });

  it('verify SBD for records by SBD Id pattern', () => {
    cy.get('[data-testid="sbdEntity"]').click();
    cy.get('[data-testid="searchId"]').type('sbd-mvp01');
    cy.get('[data-testid="idSearch"]').click();
    validateSbdDataTable();
  });

  it('verify EB records for last 7 days', () => {
    cy.get('[data-testid="ebEntity"]').click();
    cy.get('[data-testid="last7days"]').contains(translation.label.last7days);
    cy.get('[data-testid="last7days"]').click();
    cy.get('[data-testid="msgLastWeek"]').contains(translation.msg.last7days);
    validateEbDataTable();
  });

  it('verify EB records for today', () => {
    cy.get('[data-testid="ebEntity"]').click();
    cy.get('[data-testid="today"]').contains(translation.label.today);
    cy.get('[data-testid="today"]').click();
    cy.get('[data-testid="msgToday"]').contains(translation.msg.today);
    validateEbDataTable();
  });

  it('verify EB records for selected dates', () => {
    cy.get('[data-testid="ebEntity"]').click();
    cy.get('[data-testid="dateEntryStart"]').type(startDate);
    cy.get('[data-testid="dateEntryEnd"]').type(endDate);
    cy.get('[data-testid="dateSearch"]').click();
    validateEbDataTable();
  });

  it('verify EB, invalid start date selected', () => {
    cy.get('[data-testid="ebEntity"]').click();
    cy.get('[data-testid="dateEntryStart"]').type(invalidDate);
    cy.get('.MuiFormHelperText-root').contains(translation.msg.errFutureDate);
  });

  it('verify EB, invalid end date selected', () => {
    cy.get('[data-testid="ebEntity"]').click();
    cy.get('[data-testid="dateEntryEnd"]').type(invalidDate);
    cy.get('.MuiFormHelperText-root').contains(translation.msg.errFutureDate);
  });

  it('verify EB, start date greater than end date selected', () => {
    cy.get('[data-testid="ebEntity"]').click();
    cy.get('[data-testid="dateEntryStart"]').type(invalidStartDate);
    cy.get('[data-testid="dateEntryEnd"]').type(invalidEndDate);
    cy.get('.MuiFormHelperText-root').contains(translation.msg.errInvalidEndDate);
  });

  it('verify EB Id for empty Eb Id', () => {
    cy.get('[data-testid="ebEntity"]').click();
    cy.get('.MuiFormHelperText-root').contains(translation.msg.requiredEntityId);
  });

  it('verify EB for records by EB Id pattern', () => {
    cy.get('[data-testid="ebEntity"]').click();
    cy.get('[data-testid="searchId"]').type('eb-mvp01');
    cy.get('[data-testid="idSearch"]').click();
    validateEbDataTable();
  });

  it('verify Project records for last 7 days', () => {
    cy.get('[data-testid="prjEntity"]').click();
    cy.get('[data-testid="last7days"]').contains(translation.label.last7days);
    cy.get('[data-testid="last7days"]').click();
    cy.get('[data-testid="msgLastWeek"]').contains(translation.msg.last7days);
    validateProjectDataTable();
  });

  it('verify Project records for today', () => {
    cy.get('[data-testid="prjEntity"]').click();
    cy.get('[data-testid="today"]').contains(translation.label.today);
    cy.get('[data-testid="today"]').click();
    cy.get('[data-testid="msgToday"]').contains(translation.msg.today);
    validateProjectDataTable();
  });

  it('verify Project records for selected dates', () => {
    cy.get('[data-testid="prjEntity"]').click();
    cy.get('[data-testid="dateEntryStart"]').type(startDate);
    cy.get('[data-testid="dateEntryEnd"]').type(endDate);
    cy.get('[data-testid="dateSearch"]').click();
    validateProjectDataTable();
  });

  it('verify Project, invalid start date selected', () => {
    cy.get('[data-testid="prjEntity"]').click();
    cy.get('[data-testid="dateEntryStart"]').type(invalidDate);
    cy.get('.MuiFormHelperText-root').contains(translation.msg.errFutureDate);
  });

  it('verify Project, invalid end date selected', () => {
    cy.get('[data-testid="prjEntity"]').click();
    cy.get('[data-testid="dateEntryEnd"]').type(invalidDate);
    cy.get('.MuiFormHelperText-root').contains(translation.msg.errFutureDate);
  });

  it('verify Project, start date greater than end date selected', () => {
    cy.get('[data-testid="prjEntity"]').click();
    cy.get('[data-testid="dateEntryStart"]').type(invalidStartDate);
    cy.get('[data-testid="dateEntryEnd"]').type(invalidEndDate);
    cy.get('.MuiFormHelperText-root').contains(translation.msg.errInvalidEndDate);
  });

  it('verify Project Id for empty Project Id', () => {
    cy.get('[data-testid="prjEntity"]').click();
    cy.get('.MuiFormHelperText-root').contains(translation.msg.requiredEntityId);
  });

  it('verify Project for records by Project Id pattern', () => {
    cy.get('[data-testid="prjEntity"]').click();
    cy.get('[data-testid="searchId"]').type('eb-mvp01');
    cy.get('[data-testid="idSearch"]').click();
    validateProjectDataTable();
  });
});
